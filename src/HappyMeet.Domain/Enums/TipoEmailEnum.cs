﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyMeet.Domain.Enums
{
    public enum TipoEmailEnum
    {
        [Display(Description ="Após Faturamento")]
        AposFaturamento = 1,
        [Display(Description = "Antes do Vencimento")]
        AntesVencimento = 2,
        [Display(Description = "Após Vencimento")]
        AposVencimento = 3,
        [Display(Description = "Criação de Senha")]
        CriacaoSenha = 4,
        [Display(Description = "Envio Manual")]
        EnvioManual = 5
    }
}
