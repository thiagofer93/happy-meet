﻿using System;

namespace HappyMeet.Domain.Functional
{
    public static class DomainResultExtensions
    {
        public static DomainResult<K> OnSuccess<T, K>(this DomainResult<T> result, Func<T, K> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return DomainResult.Ok(func(result.Value));
        }

        public static DomainResult<T> OnSuccess<T>(this DomainResult result, Func<T> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            return DomainResult.Ok(func());
        }

        public static DomainResult<K> OnSuccess<T, K>(this DomainResult<T> result, Func<T, DomainResult<K>> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return func(result.Value);
        }

        public static DomainResult<T> OnSuccess<T>(this DomainResult result, Func<DomainResult<T>> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            return func();
        }

        public static DomainResult<K> OnSuccess<T, K>(this DomainResult<T> result, Func<DomainResult<K>> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return func();
        }

        public static DomainResult OnSuccess<T>(this DomainResult<T> result, Func<T, DomainResult> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail(result.Error);

            return func(result.Value);
        }

        public static DomainResult OnSuccess(this DomainResult result, Func<DomainResult> func)
        {
            if (result.IsFailure)
                return result;

            return func();
        }

        public static DomainResult<T> Ensure<T>(this DomainResult<T> result, Func<T, bool> predicate,
            string errorMessage)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            if (!predicate(result.Value))
                return DomainResult.Fail<T>(errorMessage);

            return DomainResult.Ok(result.Value);
        }

        public static DomainResult Ensure(this DomainResult result, Func<bool> predicate, string errorMessage)
        {
            if (result.IsFailure)
                return DomainResult.Fail(result.Error);

            if (!predicate())
                return DomainResult.Fail(errorMessage);

            return DomainResult.Ok();
        }

        public static DomainResult<K> Map<T, K>(this DomainResult<T> result, Func<T, K> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return DomainResult.Ok(func(result.Value));
        }

        public static DomainResult<T> Map<T>(this DomainResult result, Func<T> func)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            return DomainResult.Ok(func());
        }

        public static DomainResult<T> OnSuccess<T>(this DomainResult<T> result, Action<T> action)
        {
            if (result.IsSuccess)
            {
                action(result.Value);
            }

            return result;
        }

        public static DomainResult OnSuccess(this DomainResult result, Action action)
        {
            if (result.IsSuccess)
            {
                action();
            }

            return result;
        }

        public static T OnBoth<T>(this DomainResult result, Func<DomainResult, T> func)
        {
            return func(result);
        }

        public static K OnBoth<T, K>(this DomainResult<T> result, Func<DomainResult<T>, K> func)
        {
            return func(result);
        }

        public static DomainResult<T> OnFailure<T>(this DomainResult<T> result, Action action)
        {
            if (result.IsFailure)
            {
                action();
            }

            return result;
        }

        public static DomainResult OnFailure(this DomainResult result, Action action)
        {
            if (result.IsFailure)
            {
                action();
            }

            return result;
        }

        public static DomainResult<T> OnFailure<T>(this DomainResult<T> result, Action<string> action)
        {
            if (result.IsFailure)
            {
                action(result.Error);
            }

            return result;
        }

        public static DomainResult OnFailure(this DomainResult result, Action<string> action)
        {
            if (result.IsFailure)
            {
                action(result.Error);
            }

            return result;
        }
    }
}