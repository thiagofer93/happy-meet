﻿using System.Threading.Tasks;

namespace HappyMeet.Domain.Functional
{
    public static class AsyncMaybeExtensions
    {
        public static async Task<DomainResult<T>> ToResult<T>(this Task<Maybe<T>> maybeTask, string errorMessage,
            bool continueOnCapturedContext = false)
            where T : class
        {
            Maybe<T> maybe = await maybeTask.ConfigureAwait(continueOnCapturedContext);
            return maybe.ToResult(errorMessage);
        }
    }
}