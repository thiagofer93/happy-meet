﻿using System;
using System.Threading.Tasks;

namespace HappyMeet.Domain.Functional
{
    public static class AsyncDomainResultExtensionsLeftOperand
    {
        public static async Task<DomainResult<K>> OnSuccess<T, K>(this Task<DomainResult<T>> resultTask,
            Func<T, K> func, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this Task<DomainResult> resultTask, Func<T> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult<K>> OnSuccess<T, K>(this Task<DomainResult<T>> resultTask,
            Func<T, DomainResult<K>> func, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this Task<DomainResult> resultTask,
            Func<DomainResult<T>> func, bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult<K>> OnSuccess<T, K>(this Task<DomainResult<T>> resultTask,
            Func<DomainResult<K>> func, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult> OnSuccess<T>(this Task<DomainResult<T>> resultTask,
            Func<T, DomainResult> func, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult> OnSuccess(this Task<DomainResult> resultTask, Func<DomainResult> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(func);
        }

        public static async Task<DomainResult<T>> Ensure<T>(this Task<DomainResult<T>> resultTask,
            Func<T, bool> predicate, string errorMessage, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.Ensure(predicate, errorMessage);
        }

        public static async Task<DomainResult> Ensure(this Task<DomainResult> resultTask, Func<bool> predicate,
            string errorMessage, bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.Ensure(predicate, errorMessage);
        }

        public static async Task<DomainResult<K>> Map<T, K>(this Task<DomainResult<T>> resultTask, Func<T, K> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.Map(func);
        }

        public static async Task<DomainResult<T>> Map<T>(this Task<DomainResult> resultTask, Func<T> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.Map(func);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this Task<DomainResult<T>> resultTask, Action<T> action,
            bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(action);
        }

        public static async Task<DomainResult> OnSuccess(this Task<DomainResult> resultTask, Action action,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnSuccess(action);
        }

        public static async Task<T> OnBoth<T>(this Task<DomainResult> resultTask, Func<DomainResult, T> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnBoth(func);
        }

        public static async Task<K> OnBoth<T, K>(this Task<DomainResult<T>> resultTask, Func<DomainResult<T>, K> func,
            bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnBoth(func);
        }

        public static async Task<DomainResult<T>> OnFailure<T>(this Task<DomainResult<T>> resultTask, Action action,
            bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnFailure(action);
        }

        public static async Task<DomainResult> OnFailure(this Task<DomainResult> resultTask, Action action,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnFailure(action);
        }

        public static async Task<DomainResult<T>> OnFailure<T>(this Task<DomainResult<T>> resultTask,
            Action<string> action, bool continueOnCapturedContext = false)
        {
            DomainResult<T> result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnFailure(action);
        }

        public static async Task<DomainResult> OnFailure(this Task<DomainResult> resultTask, Action<string> action,
            bool continueOnCapturedContext = false)
        {
            DomainResult result = await resultTask.ConfigureAwait(continueOnCapturedContext);
            return result.OnFailure(action);
        }
    }
}