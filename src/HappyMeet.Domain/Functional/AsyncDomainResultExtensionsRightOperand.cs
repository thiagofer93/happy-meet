﻿using System;
using System.Threading.Tasks;

namespace HappyMeet.Domain.Functional
{
    public static class AsyncDomainResultExtensionsRightOperand
    {
        /// <summary>
        ///     Extentions for async operations where the task appears in the right operand only
        /// </summary>
        public static async Task<DomainResult<K>> OnSuccess<T, K>(this DomainResult<T> result, Func<T, Task<K>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            K value = await func(result.Value).ConfigureAwait(continueOnCapturedContext);

            return DomainResult.Ok(value);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this DomainResult result, Func<Task<T>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            T value = await func().ConfigureAwait(continueOnCapturedContext);

            return DomainResult.Ok(value);
        }

        public static async Task<DomainResult<K>> OnSuccess<T, K>(this DomainResult<T> result,
            Func<T, Task<DomainResult<K>>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return await func(result.Value).ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this DomainResult result,
            Func<Task<DomainResult<T>>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            return await func().ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult<K>> OnSuccess<T, K>(this DomainResult<T> result,
            Func<Task<DomainResult<K>>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            return await func().ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult> OnSuccess<T>(this DomainResult<T> result,
            Func<T, Task<DomainResult>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail(result.Error);

            return await func(result.Value).ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult> OnSuccess(this DomainResult result, Func<Task<DomainResult>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return result;

            return await func().ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult<T>> Ensure<T>(this DomainResult<T> result, Func<T, Task<bool>> predicate,
            string errorMessage, bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            if (!await predicate(result.Value).ConfigureAwait(continueOnCapturedContext))
                return DomainResult.Fail<T>(errorMessage);

            return DomainResult.Ok(result.Value);
        }

        public static async Task<DomainResult> Ensure(this DomainResult result, Func<Task<bool>> predicate,
            string errorMessage,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail(result.Error);

            if (!await predicate().ConfigureAwait(continueOnCapturedContext))
                return DomainResult.Fail(errorMessage);

            return DomainResult.Ok();
        }

        public static async Task<DomainResult<K>> Map<T, K>(this DomainResult<T> result, Func<T, Task<K>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<K>(result.Error);

            K value = await func(result.Value).ConfigureAwait(continueOnCapturedContext);

            return DomainResult.Ok(value);
        }

        public static async Task<DomainResult<T>> Map<T>(this DomainResult result, Func<Task<T>> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
                return DomainResult.Fail<T>(result.Error);

            T value = await func().ConfigureAwait(continueOnCapturedContext);

            return DomainResult.Ok(value);
        }

        public static async Task<DomainResult<T>> OnSuccess<T>(this DomainResult<T> result, Func<T, Task> action,
            bool continueOnCapturedContext = false)
        {
            if (result.IsSuccess)
            {
                await action(result.Value).ConfigureAwait(continueOnCapturedContext);
            }

            return result;
        }

        public static async Task<DomainResult> OnSuccess(this DomainResult result, Func<Task> action,
            bool continueOnCapturedContext = false)
        {
            if (result.IsSuccess)
            {
                await action().ConfigureAwait(continueOnCapturedContext);
            }

            return result;
        }

        public static async Task<T> OnBoth<T>(this DomainResult result, Func<DomainResult, Task<T>> func,
            bool continueOnCapturedContext = false)
        {
            return await func(result).ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<K> OnBoth<T, K>(this DomainResult<T> result, Func<DomainResult<T>, Task<K>> func,
            bool continueOnCapturedContext = false)
        {
            return await func(result).ConfigureAwait(continueOnCapturedContext);
        }

        public static async Task<DomainResult<T>> OnFailure<T>(this DomainResult<T> result, Func<Task> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
            {
                await func().ConfigureAwait(continueOnCapturedContext);
            }

            return result;
        }

        public static async Task<DomainResult> OnFailure(this DomainResult result, Func<Task> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
            {
                await func().ConfigureAwait(continueOnCapturedContext);
            }

            return result;
        }

        public static async Task<DomainResult<T>> OnFailure<T>(this DomainResult<T> result, Func<string, Task> func,
            bool continueOnCapturedContext = false)
        {
            if (result.IsFailure)
            {
                await func(result.Error).ConfigureAwait(continueOnCapturedContext);
            }

            return result;
        }
    }
}