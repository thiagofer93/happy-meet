﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;

namespace HappyMeet.Domain.Functional
{
    internal sealed class ResultCommonLogic
    {
        public bool IsFailure { get; }
        public bool IsSuccess => !IsFailure;
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string _error;

        public string Error
        {
            [DebuggerStepThrough]
            get
            {
                if (IsSuccess)
                    throw new InvalidOperationException("There is no error message for success.");

                return _error;
            }
        }
        
        [DebuggerStepThrough]
        public ResultCommonLogic(bool isFailure, string error)
        {
            if (isFailure)
            {
                if (string.IsNullOrEmpty(error))
                    throw new ArgumentNullException(nameof(error), "There must be error message for failure.");
            }
            else
            {
                if (error != null)
                    throw new ArgumentException("There should be no error message for success.", nameof(error));
            }

            IsFailure = isFailure;
            _error = error;
        }
    }
    
    public class DomainResult : ISerializable
    {
        private static readonly DomainResult OkResult = new DomainResult(false, null);
        
        void ISerializable.GetObjectData(SerializationInfo oInfo, StreamingContext oContext)
        {
            oInfo.AddValue("IsFailure", IsFailure);
            oInfo.AddValue("IsSuccess", IsSuccess);
            if (IsFailure)
            {
                oInfo.AddValue("Error", Error);
            }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly ResultCommonLogic _logic;

        public bool IsFailure => _logic.IsFailure;
        public bool IsSuccess => _logic.IsSuccess;
        public string Error => _logic.Error;
        
        public static implicit operator bool(DomainResult result)
        {
            return result.IsSuccess;
        }
        
        [DebuggerStepThrough]
        private DomainResult(bool isFailure, string error)
        {
            _logic = new ResultCommonLogic(isFailure, error);
        }

        [DebuggerStepThrough]
        public static DomainResult Ok()
        {
            return OkResult;
        }

        [DebuggerStepThrough]
        public static DomainResult Fail(string error)
        {
            return new DomainResult(true, error);
        }

        [DebuggerStepThrough]
        public static DomainResult<T> Ok<T>(T value)
        {
            return new DomainResult<T>(false, value, null);
        }

        [DebuggerStepThrough]
        public static DomainResult<T> Fail<T>(string error)
        {
            return new DomainResult<T>(true, default(T), error);
        }
        
        /// <summary>
        /// Returns first failure in the list of <paramref name="results"/>. If there is no failure returns success.
        /// </summary>
        /// <param name="results">List of results.</param>
        [DebuggerStepThrough]
        public static DomainResult FirstFailureOrSuccess(params DomainResult[] results)
        {
            foreach (DomainResult result in results)
            {
                if (result.IsFailure)
                    return Fail(result.Error);
            }

            return Ok();
        }
        
        /// <summary>
        /// Returns failure which combined from all failures in the <paramref name="results"/> list. Error messages are separated by <paramref name="errorMessagesSeparator"/>. 
        /// If there is no failure returns success.
        /// </summary>
        /// <param name="errorMessagesSeparator">Separator for error messages.</param>
        /// <param name="results">List of results.</param>
        [DebuggerStepThrough]
        public static DomainResult Combine(string errorMessagesSeparator, params DomainResult[] results)
        {
            List<DomainResult> failedResults = results.Where(x => x.IsFailure).ToList();

            if (!failedResults.Any())
                return Ok();

            string errorMessage = string.Join(errorMessagesSeparator, failedResults.Select(x => x.Error).ToArray());
            return Fail(errorMessage);
        }

        [DebuggerStepThrough]
        public static DomainResult Combine(params DomainResult[] results)
        {
            return Combine(", ", results);
        }
        
        [DebuggerStepThrough]
        public static DomainResult Combine(IEnumerable<DomainResult> results)
        {
            return Combine(", ", results.ToArray());
        }

        [DebuggerStepThrough]
        public static DomainResult Combine<T>(params DomainResult<T>[] results)
        {
            return Combine(", ", results);
        }
        
        [DebuggerStepThrough]
        public static DomainResult Combine<T>(IEnumerable<DomainResult<T>> results)
        {
            return Combine(", ", results);
        }

        [DebuggerStepThrough]
        public static DomainResult Combine<T>(string errorMessagesSeparator, params DomainResult<T>[] results)
        {
            DomainResult[] untyped = results.Select(result => (DomainResult)result).ToArray();
            return Combine(errorMessagesSeparator, untyped);
        }
        
        [DebuggerStepThrough]
        public static DomainResult Combine<T>(string errorMessagesSeparator, IEnumerable<DomainResult<T>> results)
        {
            DomainResult[] untyped = results.Select(result => (DomainResult)result).ToArray();
            return Combine(errorMessagesSeparator, untyped);
        }
        
        public static DomainResult From(Exception exception)
        {
            return new DomainResult(true, exception.Message);
        }
    }


    public struct DomainResult<T> : ISerializable
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly ResultCommonLogic _logic;

        public bool IsFailure => _logic.IsFailure;
        public bool IsSuccess => _logic.IsSuccess;
        public string Error => _logic.Error;

        public static implicit operator bool(DomainResult<T> result)
        {
            return result.IsSuccess;
        }

        void ISerializable.GetObjectData(SerializationInfo oInfo, StreamingContext oContext)
        {
            oInfo.AddValue("IsFailure", IsFailure);
            oInfo.AddValue("IsSuccess", IsSuccess);
            if (IsFailure)
            {
                oInfo.AddValue("Error", Error);
            }
            else
            {
                oInfo.AddValue("Value", Value);
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly T _value;

        public T Value
        {
            [DebuggerStepThrough]
            get
            {
                if (!IsSuccess)
                    throw new InvalidOperationException("There is no value for failure.");

                return _value;
            }
        }

        [DebuggerStepThrough]
        internal DomainResult(bool isFailure, T value, string error)
        {
            //if (!isFailure && value == null)
            //    throw new ArgumentNullException(nameof(value));

            _logic = new ResultCommonLogic(isFailure, error);
            _value = value;
        }

        public static implicit operator DomainResult(DomainResult<T> result)
        {
            if (result.IsSuccess)
                return DomainResult.Ok();
            else
                return DomainResult.Fail(result.Error);
        }
    }
}