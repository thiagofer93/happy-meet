﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HappyMeet.Domain.Util
{
    public static class ClaimConstants
    {
        /*
         * Cuidados ao mexer nessa classe:
         * * Todos as constants criadas aqui se transformam automaticamente em Claims no banco
         * * Todas as constants criadas aqui aparecem na tela Usuario/CreateEdit.cshtml
         * * Para funcionar bem na tela de Usuario/CreateEdit.cshtml, toda constante cadastrada aqui DEVE possuir a seguinte sintaxe: <nomedatela>.<funcao>
         * * Caso seja criada uma nova função além das listadas aqui e for necessário o usuario master isso, deverá ser feita uma alteração na tela usuario/CreateEdit.cshml e também no model PermissaoTelaViewModel para que esse parâmetro se torne visível
             */
        public const string CLIENTE_LEITURA = "cliente.leitura";
        public const string CLIENTE_ESCRITA = "cliente.escrita";
        public const string REPRESENTANTE_LEITURA = "representante.leitura";
        public const string REPRESENTANTE_ESCRITA = "representante.escrita";
        public const string USUARIO_LEITURA = "usuario.leitura";
        public const string USUARIO_ESCRITA = "usuario.escrita";
        public const string TEMPLATE_LEITURA = "template.leitura";
        public const string TEMPLATE_ESCRITA = "template.escrita";
        public const string BANCO_LEITURA = "banco.leitura";
        public const string BANCO_ESCRITA = "banco.escrita";
        public const string BOLETO_LEITURA = "boleto.leitura";
        public const string PADRAO_ENVIO_EMAIL_LEITURA = "padrao de envio de email.leitura";
        public const string PADRAO_ENVIO_EMAIL_ESCRITA = "padrao de envio de email.escrita";
        public const string LOG_EMAIL_LEITURA = "log email.leitura";
        public const string FERIADO_LEITURA = "feriado.leitura";
        public const string FERIADO_ESCRITA = "feriado.escrita";


        public static string[] CLAIMS
        {
            get
            {
                return typeof(ClaimConstants).GetFields().Where(x => x.IsStatic && x.IsLiteral).Select(x => x.GetValue(null).ToString()).ToArray();
            }
        }
    }

    public static class ClaimsCustomType
    {
        public const string PERMISSAO = "permissao";
    }
}
