﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Util
{
    public static class RoleConstants
    {
        public const string ADMIN = "admin";
        public const string SUPER_USER = "super_user";
        public const string USER = "user";
    }
}
