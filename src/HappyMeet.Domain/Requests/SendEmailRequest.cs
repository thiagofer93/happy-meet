﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class SendEmailRequest : IRequest<DomainResult>
    {
        public string[] EmailTo { get; }
        public string Subject { get; }
        public string Message { get; }
        public string EmailFrom { get; set; }

        public SendEmailRequest(string emailTo, string subject, string message) : this(new string[] { emailTo }, subject, message)
        {
        }
        public SendEmailRequest(string[] emailTo, string subject, string message)
        {
            EmailTo = emailTo;
            Subject = subject;
            Message = message;
        }
    }
}
