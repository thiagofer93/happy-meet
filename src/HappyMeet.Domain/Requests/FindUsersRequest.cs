﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindUsersRequest : BaseFindRequest<ApplicationUser>, IRequest<DomainResult<List<ApplicationUser>>>
    {
        public FindUsersRequest() : this(x => true)
        {
        }

        public FindUsersRequest(Expression<Func<ApplicationUser, bool>> exp) : this(exp, "", "", 0, 0)
        {
        }

        public FindUsersRequest(Expression<Func<ApplicationUser, bool>> searchExpression, string sortProperty, string sortOrder, int page, int pageSize)
            : base(searchExpression, sortProperty, sortOrder, page, pageSize)
        {
        }
    }
}
