using System;
using MediatR;

namespace HappyMeet.Domain.Requests
{
    public class GetProximoDiaUtilRequest : IRequest<DateTime>
    {
        public GetProximoDiaUtilRequest(DateTime data, string uf)
        {
            Data = data;
            Uf = uf;
        }

        public DateTime Data { get; }
        public string Uf { get; }
    }
}