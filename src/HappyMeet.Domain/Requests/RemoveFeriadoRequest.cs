﻿using MediatR;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class RemoveFeriadoRequest : IRequest<DomainResult>
    {
        public int Id { get; }

        public RemoveFeriadoRequest(int id)
        {
            Id = id;
        }
    }
}
