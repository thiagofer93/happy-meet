﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindRolesAndClaimsByUserRequest : IRequest<DomainResult<Tuple<IList<string>, IList<Claim>>>>
    {
        public ApplicationUser User { get; }

        public FindRolesAndClaimsByUserRequest(ApplicationUser user)
        {
            User = user;
        }
    }
}
