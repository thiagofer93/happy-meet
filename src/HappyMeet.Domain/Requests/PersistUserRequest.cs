﻿using MediatR;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class PersistUserRequest : IRequest<DomainResult<string>>
    {
        public string UserName { get; }
        public string Password { get; }
        public string Nome { get; }
        public string Email { get; }
        public string Role { get;}
        public bool Ativo { get;}
        public string[] Claims { get;}
        public bool SendEmail { get; }

        public PersistUserRequest(string userName, string password, string nome, string email, bool ativo, string role, string[] claims = null, bool sendEmail = true)
        {
            UserName = userName;
            Password = password;
            Nome = nome;
            Email = email;
            Ativo = ativo;
            Role = role;
            Claims = claims;
            SendEmail = sendEmail;
        }
    }
}
