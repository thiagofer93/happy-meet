﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class BaseFindRequest<T>
    {
        public Expression<Func<T, bool>> SearchExpression { get; }
        public string SortProperty { get; }
        public string SortOrder { get; }
        public int Page { get; }
        public int PageSize { get; set; }
        public int PageIndex { get { return Page == 0 ? 0 : Page - 1; } }
        public int PageSkip { get { return PageIndex * PageSize; } }

        public BaseFindRequest(Expression<Func<T, bool>> searchExpression, string sortProperty, string sortOrder, int page, int pageSize)
        {
            SearchExpression = searchExpression;
            SortProperty = sortProperty;
            SortOrder = sortOrder;
            Page = page;
            PageSize = pageSize;
        }
    }
}
