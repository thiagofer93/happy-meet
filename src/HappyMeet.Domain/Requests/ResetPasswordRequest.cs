﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class ResetPasswordRequest : IRequest<DomainResult>
    {
        public ApplicationUser User { get; }

        public ResetPasswordRequest(ApplicationUser user)
        {
            User = user;
        }
    }
}
