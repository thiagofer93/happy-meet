﻿using MediatR;
using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindBancoCountRequest : IRequest<int>
    {
        public Expression<Func<Banco, bool>> Expression { get; }

        public FindBancoCountRequest() : this(x => true)
        {

        }
        public FindBancoCountRequest(Expression<Func<Banco, bool>> exp)
        {
            Expression = exp;
        }
    }
}
