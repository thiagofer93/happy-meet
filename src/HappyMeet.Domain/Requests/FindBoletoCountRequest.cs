﻿using MediatR;
using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindBoletoCountRequest : IRequest<int>
    {
        public Expression<Func<Boleto, bool>> Expression { get; }

        public FindBoletoCountRequest() : this(x => true)
        {

        }
        public FindBoletoCountRequest(Expression<Func<Boleto, bool>> exp)
        {
            Expression = exp;
        }
    }
}
