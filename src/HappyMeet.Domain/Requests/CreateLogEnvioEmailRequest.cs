﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Enums;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class CreateLogEnvioEmailRequest : IRequest<DomainResult>
    {
        public string[] EmailsDestino { get; }
        public Boleto[] Boletos { get; }
        public TipoEmailEnum TipoEmail { get; }

        public CreateLogEnvioEmailRequest(string[] emailsDestino, Boleto[] boletos, TipoEmailEnum tipoEmail)
        {
            EmailsDestino = emailsDestino;
            Boletos = boletos;
            TipoEmail = tipoEmail;
        }
    }
}
