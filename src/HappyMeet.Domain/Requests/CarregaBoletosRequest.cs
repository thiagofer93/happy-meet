﻿using System.Collections.Generic;
using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;

namespace HappyMeet.Domain.Requests
{
    public class CarregaBoletosRequest : IRequest<DomainResult>
    {
        public readonly IEnumerable<Boleto> Dados;
        public readonly IEnumerable<(string CpfCnpj, string Email, string Nome)> Clientes;

        public CarregaBoletosRequest(IEnumerable<Boleto> dados, IEnumerable<(string CpfCnpj, string Email, string Nome)> clientes)
        {
            Dados = dados;
            Clientes = clientes;
        }
    }
}