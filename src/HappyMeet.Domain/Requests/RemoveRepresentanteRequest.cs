﻿using MediatR;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class RemoveRepresentanteRequest : IRequest<DomainResult>
    {
        public int Id { get; }

        public RemoveRepresentanteRequest(int id)
        {
            Id = id;
        }
    }
}
