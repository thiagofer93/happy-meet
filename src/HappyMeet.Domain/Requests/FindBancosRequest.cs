﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindBancosRequest : BaseFindRequest<Banco>, IRequest<DomainResult<List<Banco>>>
    {
        public FindBancosRequest() : this(x => true)
        {

        }
        public FindBancosRequest(Expression<Func<Banco, bool>> exp) : this(exp, "", "", 0, 0)
        {

        }
        public FindBancosRequest(Expression<Func<Banco, bool>> searchExpression, string sortProperty, string sortOrder, int page, int pageSize)
            : base(searchExpression, sortProperty, sortOrder, page, pageSize)
        {
        }
    }
}
