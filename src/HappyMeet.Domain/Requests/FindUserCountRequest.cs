﻿using MediatR;
using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindUserCountRequest : IRequest<int>
    {
        public Expression<Func<ApplicationUser, bool>> Expression { get; }

        public FindUserCountRequest() : this(x => true)
        {

        }
        public FindUserCountRequest(Expression<Func<ApplicationUser, bool>> exp)
        {
            Expression = exp;
        }
    }
}
