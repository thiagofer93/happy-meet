﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class PersistConfiguracaoRequest : IRequest<DomainResult<string>>
    {

        public Configuracao Configuracao { get; }

        public PersistConfiguracaoRequest(Configuracao configuracao)
        {
            Configuracao = configuracao;
        }
    }
}
