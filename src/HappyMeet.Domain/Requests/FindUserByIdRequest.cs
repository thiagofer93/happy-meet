﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindUserByIdRequest : IRequest<DomainResult<ApplicationUser>>
    {
        public int Id { get; }

        public FindUserByIdRequest(int id)
        {
            Id = id;
        }
    }
}
