﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class FindBoletosRequest : BaseFindRequest<Boleto>, IRequest<DomainResult<List<Boleto>>>
    {
        public FindBoletosRequest() : this(x => true)
        {

        }
        public FindBoletosRequest(Expression<Func<Boleto, bool>> exp) : this(exp, "", "", 0, 0)
        {

        }
        public FindBoletosRequest(Expression<Func<Boleto, bool>> searchExpression, string sortProperty, string sortOrder, int page, int pageSize)
            : base(searchExpression, sortProperty, sortOrder, page, pageSize)
        {
        }
    }
}
