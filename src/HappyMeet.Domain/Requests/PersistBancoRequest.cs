﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Requests
{
    public class PersistBancoRequest : IRequest<DomainResult<string>>
    {
        public Banco Banco { get; }

        public PersistBancoRequest(Banco banco)
        {
            Banco = banco;
        }

    }
}
