﻿using MediatR;
using Microsoft.Extensions.Configuration;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Domain.Services
{
    public class SendEmailHandler : IRequestHandler<SendEmailRequest, DomainResult>
    {
        private readonly AppConfig _config;

        public SendEmailHandler(AppConfig config)
        {
            _config = config;
        }

        public Task<DomainResult> Handle(SendEmailRequest request, CancellationToken cancellationToken)
        {
            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = _config.EmailUsername,
                    Password = _config.EmailPassword
                };

                client.Credentials = credential;
                client.Host = _config.EmailSmtp;
                client.Port = _config.EmailPort;
                client.EnableSsl = _config.EmailEnableSsl;
                client.Timeout = 20000;

                using (var emailMessage = new MailMessage())
                {
                    foreach (var email in request.EmailTo)
                    {
                        emailMessage.To.Add(email);
                    }

                    string message = request.Message;

                    string emailFrom = request.EmailFrom ?? _config.EmailUsername;
                    emailMessage.From = new MailAddress(emailFrom);
                    emailMessage.Subject = request.Subject;
                    emailMessage.Body = message;
                    emailMessage.IsBodyHtml = true;

                    client.Send(emailMessage);
                }
            }

            return Task.FromResult(DomainResult.Ok());
        }
    }
}
