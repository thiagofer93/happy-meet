﻿using MediatR;
using Nito.AsyncEx;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Services
{
    public class AppConfig
    {
        public string NomeCliente { get; }
        public string CorPrimaria { get; }
        public string CorSecundaria { get; }
        public string CorFonte { get; }
        public byte[] ImagemLogo { get; }
        public byte[] ImagemFundo { get; }
        public string EmailSmtp { get; }
        public string EmailFrom { get; }
        public int EmailPort { get; }
        public string EmailUsername { get; }
        public string EmailPassword { get; }
        public bool EmailEnableSsl { get; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public string RamalContato { get; set; }


        public string ImagemLogoBase64
        {
            get
            {
                var base64 = ImagemLogo == null ? "" : Convert.ToBase64String(ImagemLogo);
                return String.Format("data:image/png;base64,{0}", base64);
            }
        }
        public string ImageFundoBase64
        {
            get
            {
                var base64 = ImagemFundo == null ? "" : Convert.ToBase64String(ImagemFundo);
                return String.Format("data:image/png;base64,{0}", base64);
            }
        }

        public AppConfig(IMediator mediator)
        {
            var config = AsyncContext.Run(() => mediator.Send(new FindConfiguracaoRequest())).Value;
            if (config != null)
            {
                NomeCliente = config.NomeCliente;
                CorPrimaria = config.CorPrimaria;
                CorSecundaria = config.CorSecundaria;
                CorFonte = config.CorFonte;
                ImagemLogo = config.ImagemLogo;
                ImagemFundo = config.ImagemFundo;
                EmailSmtp = config.EmailSmtp;
                EmailFrom = config.EmailFrom;
                EmailPort = config.EmailPort;
                EmailUsername = config.EmailUsername;
                EmailPassword = config.EmailPassword;
                EmailEnableSsl = config.EmailEnableSsl;
                EmailContato = config.EmailContato;
                TelefoneContato = config.TelefoneContato;
                RamalContato = config.RamalContato;
            }
        }
    }
}
