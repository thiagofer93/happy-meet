﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HappyMeet.Domain.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// ForEach extension that enumerates over all items in an <see cref="IEnumerable{T}"/> and executes 
        /// an action.
        /// </summary>
        /// <typeparam name="T">The type that this extension is applicable for.</typeparam>
        /// <param name="collection">The enumerable instance that this extension operates on.</param>
        /// <param name="action">The action executed for each iten in the enumerable.</param>
        [DebuggerNonUserCode]
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (T item in collection)
                action(item);
        }
        
        [DebuggerNonUserCode]
        public static async Task ForEach<T>(this IEnumerable<T> collection, Func<T, Task> asyncAction)
        {
            foreach (T item in collection)
                await asyncAction(item);
        }

        /// <summary>
        /// ForEach extension that enumerates over all items in an <see cref="IEnumerator{T}"/> and executes 
        /// an action.
        /// </summary>
        /// <typeparam name="T">The type that this extension is applicable for.</typeparam>
        /// <param name="collection">The enumerator instance that this extension operates on.</param>
        /// <param name="action">The action executed for each iten in the enumerable.</param>
        [DebuggerNonUserCode]
        public static void ForEach<T>(this IEnumerator<T> collection, Action<T> action)
        {
            while (collection.MoveNext())
                action(collection.Current);
        }
        
        [DebuggerNonUserCode]
        public static async Task ForEach<T>(this IEnumerator<T> collection, Func<T, Task> asyncAction)
        {
            while (collection.MoveNext())
                await asyncAction(collection.Current);
        }
    }
}