﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Extensions
{
    public static class DateExtensions
    {
        public static DateTime ToEndOfTheDay(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }
    }
}
