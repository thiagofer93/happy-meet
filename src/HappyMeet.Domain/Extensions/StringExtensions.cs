﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Globalization;

namespace HappyMeet.Domain.Extensions
{
    public static class StringExtensions
    {
        public static string ToSnakeCase(this string str)
        {
            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
        }

        public static string OnlyDigits(this string str)
        {
            return new String(str.Where(Char.IsDigit).ToArray());
        }

        public static string ToTitleCase(this string str)
        {
            TextInfo textInfo = new CultureInfo("pt-BR", false).TextInfo;


            return textInfo.ToTitleCase(str.ToLower());
        }

        public static string SubstituirVariavel(this string str, string variavel, string valor)
        {
            return str.Replace($"{{{{{variavel}}}}}", valor);
        }
    }
}
