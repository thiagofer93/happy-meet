﻿using System;
using System.Linq;

namespace HappyMeet.Domain.Extensions
{
    public static class TypeExtension
    {
        /// <summary>
		/// Returns <c>true</c> if the type either derives from the given type (if the given type is a class or struct),
		/// or implements the given interface, whether it be directly or indirectly through its inheritance hierarchy.
		/// </summary>
		/// <param name="child">The type being evaluated for the result.</param>
		/// <typeparam name="TParent">The type to compare to.</typeparam>
		public static bool InheritsOrImplements<TParent>(this Type child)
		{
			return InheritsOrImplements(child, typeof(TParent));
		}

		/// <summary>
		/// Returns <c>true</c> if the type either derives from the given type (if the given type is a class or struct),
		/// or implements the given interface, whether it be directly or indirectly through its inheritance hierarchy.
		/// </summary>
		/// <param name="child">The type being evaluated for the result.</param>
		/// <param name="parent">The type to compare to.</param>
		public static bool InheritsOrImplements(this Type child, Type parent)
		{
			if (child == null || parent == null)
				return false;

			parent = ResolveGenericTypeDefinition(parent);

			if (parent.IsAssignableFrom(child))
				return true;

			var currentChild = child.IsGenericType
				? child.GetGenericTypeDefinition()
				: child;

			while (currentChild != typeof(object))
			{
				if (parent == currentChild || HasAnyInterfaces(parent, currentChild))
					return true;

				currentChild = currentChild.BaseType != null && currentChild.BaseType.IsGenericType
					? currentChild.BaseType.GetGenericTypeDefinition()
					: currentChild.BaseType;

				if (currentChild == null)
					return false;
			}

			return false;
		}

		static bool HasAnyInterfaces(Type parent, Type child)
		{
			return child.GetInterfaces().Any(childInterface =>
			{
				var currentInterface = childInterface.IsGenericType
					? childInterface.GetGenericTypeDefinition()
					: childInterface;

				return currentInterface == parent;
			});
		}

		static Type ResolveGenericTypeDefinition(Type type)
		{
			var shouldUseGenericType = !(type.IsGenericType && type.GetGenericTypeDefinition() != type);
			if (type.IsGenericType && shouldUseGenericType)
				type = type.GetGenericTypeDefinition();
			return type;
		}
    }
}