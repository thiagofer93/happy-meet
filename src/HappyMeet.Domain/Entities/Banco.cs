﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HappyMeet.Domain.Entities
{
    public class Banco
    {
        [Key]
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string NomeBanco { get; set; }
        public byte[] Arquivo { get; set; }
        public string NomeArquivo { get; set; }
        public bool Ativo { get; set; }
    }
}
