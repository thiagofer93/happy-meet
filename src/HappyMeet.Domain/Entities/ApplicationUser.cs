﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace HappyMeet.Domain.Entities
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string Nome { get; set; }
        public bool Ativo { get; set; }
    }
}
