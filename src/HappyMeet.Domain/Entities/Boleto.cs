﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HappyMeet.Domain.Entities
{
    public class Boleto
    {
        public int Id { get; set; }
        public string CpfCnpjBenificiario { get; set; }
        public string CpfCnpjPagador { get; set; }
        public string NomeBeneficiario { get; set; }
        public string NomePagador { get; set; }
        public string NotaFiscal { get; set; }
        public string NumDocumento { get; set; }
        public DateTime DtEmissao { get; set; }
        public DateTime DtVencimento { get; set; }
        public DateTime DtVencimentoAtualizado { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorMulta { get; set; }
        public decimal ValorJuroDia { get; set; }
        public decimal ValorAbatimento { get; set; }
        public string NossoNumero { get; set; }
        public string DigNossoNumero { get; set; }
        public decimal ValorAtualizado { get; set; }
        public string CodRepresentante { get; set; }
        public string Observacao1 { get; set; }
        public string Observacao2 { get; set; }
        public string Observacao3 { get; set; }
        public int CodBanco { get; set; }
        public string Agencia { get; set; }
        [StringLength(2)]
        public string DigAgencia { get; set; }
        public string Conta { get; set; }
        [StringLength(2)]
        public string DigConta { get; set; }
        public string CodCedente { get; set; }
        public int Carteira { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        [StringLength(2)]
        public string Uf { get; set; }
        public DateTime DtProcessamento { get; set; }
        public string EnderecoBeneficiario { get; set; }
        public string LinhaDigitavel { get; set; }
        public string CodigoBarras { get; set; }
        public DateTime DtVencimentoReal { get; set; }
        public DateTime DtIntegracao { get; set; }
    }
}
