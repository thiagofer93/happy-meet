﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyMeet.Domain.Entities
{
    public class Configuracao
    {
        public int Id { get; set; }
        public string NomeCliente { get; set; }
        public string CorPrimaria { get; set; }
        public string CorSecundaria { get; set; }
        public string CorFonte { get; set; }
        public byte[] ImagemLogo { get; set; }
        public string NomeLogo { get; set; }
        public byte[] ImagemFundo { get; set; }
        public string NomeFundo { get; set; }
        public string EmailSmtp { get; set; }
        public string EmailFrom { get; set; }
        public int EmailPort { get; set; }
        public string EmailUsername { get; set; }
        public string EmailPassword { get; set; }
        public bool EmailEnableSsl { get; set; }
        public bool EnviaEmailBoleto { get; set; }
        public bool EnviaApenasClienteParametrizado { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public string RamalContato { get; set; }
    }
}
