﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HappyMeet.Domain.Entities
{
    public class Empresa
    {
        [Key]
        public virtual int Id { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(18)]
        public virtual string CpfCnpj { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        public virtual string Denominacao { get; set; }
    }
}
