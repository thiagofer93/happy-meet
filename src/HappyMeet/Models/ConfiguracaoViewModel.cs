﻿using HappyMeet.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Models
{
    public class ConfiguracaoViewModel
    {
        public int Id { get; set; }
        [DefaultRequired]
        public string NomeCliente { get; set; }
        public string CorPrimaria { get; set; }
        public string CorSecundaria { get; set; }
        public string CorFonte { get; set; }
        public byte[] LogoCliente { get; set; }
        public string EmailSmtp { get; set; }
        public string EmailFrom { get; set; }
        public string EmailPort { get; set; }
        public string EmailUsername { get; set; }
        public string EmailPassword { get; set; }
        public string EmailEnableSsl { get; set; }
        public bool EnviaEmailBoleto { get; set; }
        public bool EnviaApenasClienteParametrizado { get; set; }
        public string EmailContato { get; set; }
        public string TelefoneContato { get; set; }
        public string RamalContato { get; set; }
    }
}
