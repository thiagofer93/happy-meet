﻿using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace HappyMeet.Models
{
    public class BancoPresentationViewModel : PresentationViewModel
    {
        public Banco[] Bancos { get; set; }
        public IPagedList<Banco> BancosPaged { get; set; }
    }
}
