﻿using HappyMeet.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Models
{
    public class ResetPasswordViewModel
    {
        [DefaultRequired]
        public int UserId { get; set; }

        [DefaultRequired]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Compare("Senha", ErrorMessage = "As senhas não estão iguais")]
        public string RepetirSenha { get; set; }

        public string Code { get; set; }
    }
}
