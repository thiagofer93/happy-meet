﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Models
{
    public class PresentationViewModel
    {
        public virtual int TotalCount { get; set; }
        public virtual int CurrentPage { get; set; }
        public virtual int RegistrosPorPagina { get; set; }
    }
}
