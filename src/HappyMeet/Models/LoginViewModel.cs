﻿using HappyMeet.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Models
{
    public class LoginViewModel
    {
        [DefaultRequired]
        public string UserName { get; set; }

        [DefaultRequired]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
