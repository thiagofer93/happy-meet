﻿using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace HappyMeet.Models
{
    public class BoletoViewModel : PresentationViewModel
    {
        public decimal TotalGeral { get; set; }
        public decimal TotalAtraso { get; set; }
        public decimal TotalHoje { get; set; }
        public Boleto[] Boletos { get; set; }
        public IPagedList<Boleto> BoletosPaged { get; set; }
        public List<string> Emails { get; set; }
    }
}
