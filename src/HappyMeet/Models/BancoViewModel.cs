﻿using HappyMeet.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Models
{
    public class BancoViewModel
    {
        public int Id { get; set; }
        [DefaultRequired]
        public int Codigo { get; set; }
        [DefaultRequired]
        public string NomeBanco { get; set; }
        public byte[] Arquivo { get; set; }
        public string NomeArquivo { get; set; }
        public bool Ativo { get; set; }
        public bool PossuiArquivo => Arquivo != null && Arquivo.Length > 0 && Id > 0;
    }
}
