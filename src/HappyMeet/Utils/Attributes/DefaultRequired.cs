﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Utils.Attributes
{
    public class DefaultRequired : RequiredAttribute
    {
        public DefaultRequired()
        {
            this.ErrorMessage = "Este campo é obrigatório";
        }
    }
}
