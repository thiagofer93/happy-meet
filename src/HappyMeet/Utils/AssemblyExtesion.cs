﻿using System.Reflection;

namespace HappyMeet.Utils
{
    public static class AssemblyExtesion
    {
        public static string GetVersion(this Assembly a)
        {
            return Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
        }
    }
}
