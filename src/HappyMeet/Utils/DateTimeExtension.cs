﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace HappyMeet.Utils
{
    public static class DateTimeExtension
    {
        public static string ClientTimezoneOffset { get; set; }

        private static int? ClientTimezoneOffsetInt
        {
            get
            {
                int offset = 0;
                if (int.TryParse(ClientTimezoneOffset, out offset))
                    return offset;

                return null;
            }
        }

        public static string GetHourFromIntMinutes(this int num)
        {
            return TimeSpan.FromMinutes((double)num).ToString(@"hh\:mm");
        }

        public static int GetMinutesFromStringHours(this string hour)
        {
            var ts = TimeSpan.ParseExact(hour, @"hh\:mm", CultureInfo.InvariantCulture);
            return Convert.ToInt32(ts.TotalMinutes);
        }

        public static string ToDateString(this DateTime? date, string format = "dd/MM/yyyy")
        {
            if (!date.HasValue)
            {
                return String.Empty;
            }

            return date.Value.ToString(format);
        }

        public static DateTime ToClientDateTime(this DateTime dt)
        {
            if (ClientTimezoneOffsetInt.HasValue)
            {
                dt = dt.AddMinutes(-1 * ClientTimezoneOffsetInt.Value);
            }

            return dt;
        }

        public static DateTime? ToClientDateTime(this DateTime? dt)
        {
            if (dt.HasValue)
            {
                return ToClientDateTime(dt.Value);
            }

            return null;
        }

        public static string ToClientDateString(this DateTime dt, string format = "dd/MM/yyyy")
        {
            return ToClientDateTime(dt).ToString(format);
        }

        public static string ToClientDateString(this DateTime? date, string format = "dd/MM/yyyy")
        {
            if (!date.HasValue)
            {
                return String.Empty;
            }

            return ToClientDateString(date.Value, format);
        }

        public static DateTime GetBrazilianDateAsUtc(this DateTime dt)
        {
            var kindDate = DateTime.SpecifyKind(dt, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(kindDate, "E. South America Standard Time", "UTC");
        }

        public static DateTime? ToUniversalTime(this DateTime? dt)
        {
            return dt.HasValue ? dt.Value.ToUniversalTime() : (DateTime?)null;
        }

        public static DateTime? Date(this DateTime? dt)
        {
            return dt.HasValue ? dt.Value.Date : (DateTime?)null;
        }
    }
}
