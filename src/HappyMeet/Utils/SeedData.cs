﻿using Microsoft.AspNetCore.Identity;
using Nito.AsyncEx;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using HappyMeet.Repository;
using Microsoft.EntityFrameworkCore;

namespace HappyMeet.Utils
{
    public static class SeedData
    {

        public static async Task Initialize(IServiceProvider provider)
        {
            var context = provider.GetService<PostgresContext>();
            var userManager = provider.GetService<UserManager<ApplicationUser>>();
            var roleManager = provider.GetService<RoleManager<ApplicationRole>>();

            //await context.Database.MigrateAsync();

            var roleAdmin = await roleManager.FindByNameAsync(RoleConstants.ADMIN);

            if (roleAdmin == null)
            {
                roleAdmin = new ApplicationRole();
                roleAdmin.Name = RoleConstants.ADMIN;
                await roleManager.CreateAsync(roleAdmin);
            }

            var allAdminClaims = await roleManager.GetClaimsAsync(roleAdmin);

            foreach (var claim in ClaimConstants.CLAIMS)
            {
                if (!allAdminClaims.Select(x => x.Value).Contains(claim))
                {
                    await roleManager.AddClaimAsync(roleAdmin, new Claim(ClaimsCustomType.PERMISSAO, claim));
                }
            }

            var roleSuperUser = await roleManager.FindByNameAsync(RoleConstants.SUPER_USER);

            if (roleSuperUser == null)
            {
                roleSuperUser = new ApplicationRole();
                roleSuperUser.Name = RoleConstants.SUPER_USER;
                await roleManager.CreateAsync(roleSuperUser);
            }

            var roleUser = await roleManager.FindByNameAsync(RoleConstants.USER);

            if (roleUser == null)
            {
                roleUser = new ApplicationRole();
                roleUser.Name = RoleConstants.USER;
                await roleManager.CreateAsync(roleUser);
            }

            var allUserClaims = await roleManager.GetClaimsAsync(roleUser);

            if (!allUserClaims.Select(x => x.Value).Contains(ClaimConstants.BOLETO_LEITURA))
            {
                await roleManager.AddClaimAsync(roleUser, new Claim(ClaimsCustomType.PERMISSAO, ClaimConstants.BOLETO_LEITURA));
            }

            var user = await userManager.FindByNameAsync("admin");

            if (user == null)
            {
                user = new ApplicationUser();
                user.UserName = "admin";
                user.Nome = "Admin";
                user.Ativo = true;
                var result = await userManager.CreateAsync(user, "Admin@123");

                await userManager.AddToRoleAsync(user, RoleConstants.ADMIN);
            }
        }
    }


}
