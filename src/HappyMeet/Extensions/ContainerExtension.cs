﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using HappyMeet.Domain.Extensions;
using SimpleInjector;

namespace HappyMeet.Extensions
{
    public static class ContainerExtension
    {
        private static readonly ConcurrentDictionary<Type,object> EmptyDic = new ConcurrentDictionary<Type, object>();
        
        public static Container RegisterHandler<TRequestHandler>(this Container container, Lifestyle lifestyle) where TRequestHandler : class
        {
            foreach (var type in typeof(TRequestHandler).GetInterfaces().Where(x =>
                x.InheritsOrImplements(typeof(IRequestHandler<>)) || 
                x.InheritsOrImplements(typeof(IRequestHandler<,>)) || 
                x.InheritsOrImplements(typeof(INotificationHandler<>))))
            {
                container.Register(type, typeof(TRequestHandler), lifestyle);
            }

            return container;
        }

        public static Container RegisterMediatR(this Container container)
        {
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(
                () => new ServiceFactory(t =>
                {
                    if (!t.InheritsOrImplements(typeof(IEnumerable<>)))
                    {
                        return container.GetInstance(t);
                    }
                    
                    IServiceProvider provider = container;
                    var services = provider.GetService(t);
                    if (services != null)
                        return services;

                    var empty = EmptyDic.GetOrAdd(t,x=> typeof(Enumerable).GetMethod("Empty")
                        ?.MakeGenericMethod(x.GetGenericArguments()[0])
                        .Invoke(null, null));

                    return empty;

                }), Lifestyle.Singleton);
            return container;
        }
    }
}