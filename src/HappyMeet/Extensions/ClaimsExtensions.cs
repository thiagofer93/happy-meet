﻿using HappyMeet.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace HappyMeet.Extensions
{
    public static class ClaimsExtensions
    {
        public static bool HasPermission(this IPrincipal principal, string claim)
        {
            return ((ClaimsIdentity)principal.Identity).HasClaim(ClaimsCustomType.PERMISSAO, claim);
        }
    }
}
