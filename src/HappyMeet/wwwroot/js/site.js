﻿$(document).ready(function(){

	setDefaultScripts();
	checkForCreateEdit();
	checkForErrorInputs();
	dataHrefLinks();
	preventCloseDropdownFlatPickr();
	setHoverActiveColors();
    initFiltros();
    setTimezoneCookie();

	changePaginationClasses();

	$('body').on('click', '.dropdown-menu.show', e => e.stopPropagation());

	$('body').on('click', '.js-clear', function(e){
		e.stopPropagation();

		var _this 	= $(this),
			$input 	= _this.closest('.with-icon').find('.flatpickr-input')[0]._flatpickr;

		$input.clear();
    });

    $('body').on('click', '#open-modal-contato', function () {
        $('#contato').modal('show')
    })

	function changePaginationClasses(){
		var $pagination 	= $('#pagination')
			hasPagination 	= $pagination.length > 0;

		if(hasPagination){
			var itens 		= $pagination.find('li'),
				hasActive 	= $pagination.find('li.active').length > 0;

			$.each(itens, function(index, object){
				var _this 	= $(object),
					isActive= _this.hasClass('active'),
					cssClass= isActive || (!hasActive && index == 1) ? 'page-item active' : 'page-item';

				_this.addClass(cssClass)
						.children(isActive ? 'span' : 'a').addClass('page-link')
			})
		}
	}

	function getSortedProperty(){
		return $('input[name="sortOrder"]:checked').closest('th').data('sort');
	}

	function getOrderProperty(){
		return $('input[name="sortOrder"]:checked').val();
	}

	function initFiltros(){

		var template 		= $('#templateOrder').html(),
			$tableHeader 	= $('.js-ajax-table-header');

		// init
		$.each($tableHeader.find('th'), function(index, object){
			var _this 		= $(this),
				sort 		= _this.data('sort'),
				hasSortAttr	= Boolean(sort),
                defaultSort = _this.attr('data-defaultsort'),
                ascendingCheck = defaultSort == "ascending" ? 'checked' : "",
                descendingCheck = defaultSort == "descending" ? 'checked' : "",
                html = defaultSort ? template.fakeHandlebars({ ascendingChecked: ascendingCheck, descendingChecked: descendingCheck}) : template

			if(hasSortAttr){
				_this.append(html);
			} 
		});

		// events
		$('body').on('submit', '.dropdown-filtros-menu form', function(){
			updateFiltro({ pagina: 1 });
			return false;
		});

		$('body').on('click', 'th[data-sort]', function(){
			var _this 		= $(this),
				checkedInput= _this.find('input:checked'),
				hasAnyOrder = checkedInput.length > 0;

			if(hasAnyOrder){
				_this.find('input').not(':checked').prop('checked', true);
				checkedInput.prop('checked', false);
			} else {
				_this.find('input').first().prop('checked', true)
			}

			updateFiltro({ pagina: 1 });
		});

		$('body').on('click', '#pagination .page-link', function(){
			var _this 		= $(this),
				pageNumber 	= checkForPageNumber(_this);

			updateFiltro({ pagina: pageNumber });
			return false;
		});

		function checkForPageNumber(_this){
			var number 		= 1,
				$container 	= _this.closest('#pagination'),
				isNumber	= !isNaN(_this.text()),
				isNext 		= _this.closest('li').hasClass('PagedList-skipToNext'),
				isPrev 		= _this.closest('li').hasClass('PagedList-skipToPrevious'),
				isFirstPage = _this.closest('li').hasClass('PagedList-skipToFirst'),
				isLastPage  = _this.closest('li').hasClass('PagedList-skipToLast');

			if(isNumber){
				number = _this.text();
			}

			if(isNext || isPrev){
				var current = Number($container.find('.active .page-link').text());
				number = isNext ? current + 1 : current - 1;
			}

			if(isFirstPage){
				number = 1;
			}

			if(isLastPage){
				var url = _this.attr('href');
				number = url.split('pagina=')[1]
			}

			return number;
		}

	}

	function updateFiltro(extendObject){
		var object 	= extendObject || {},
			$table 	= $('.js-ajax-table'),
			$pagin 	= $('#pagination'),
			$check 	= $('.js-ajax-table-header').find(':checkbox'),
			sort 	= getSortedProperty(),
			order 	= getOrderProperty(),
			filtro 	= $('.dropdown-filtros-menu form').serializeObject(),
			page 	= object.pagina || 1,
			serial  = $.extend(filtro, { sortProperty: sort, sortOrder: order, pagina: page }, object);

		// fix valor
		if(serial.searchValor){
			serial.searchValor = serial.searchValor.toNumber();
		}

		$table.loading('loading');
		$.get(searchUrl, serial, function(data){
			var $html 	= $(data),
				content = $html.find('.js-ajax-table').html(),
				pagin 	= $html.find('#pagination').html();

			$pagin.html(pagin);
			$pagin.find('.page-item.active').removeClass('active');
			$pagin.find('.page-item').eq(page - 1).addClass('active');
			$table.loading('loaded');
			$table.html(content);

			changePaginationClasses();
			if($check.length > 0){
				$check.attr('checked', false).trigger('change');
			}
		})

	}

	function preventCloseDropdownFlatPickr(){
		$('body').on('click', '.flatpickr-calendar', function(e){
			var isDropdownOpen = $('.dropdown-menu.show').length > 0;
			if(isDropdownOpen){
				e.stopPropagation();
			}
			return false
		})
	}

	function dataHrefLinks(){
		$('body').on('click mousedown', '[data-href]', debounce(function(e){
	        e.preventDefault();

	        var $that = $(this),
	        	isLink 	= e.target.localName === "a",
	        	isMiddleClick = (e.type == "mousedown" && e.which === 2),
	            url = $that.attr('data-href'),
	            hasClass = (url[0] == "." || url[0] == "#");

	        if(isLink){
	        	return;
	        }

	        if(isMiddleClick){
	        	var path = hasClass ? $that.find(url)[0].attr('href') : url;
	        	window.open(path, '_blank');
	        } else {
	        	if(hasClass){
	        		$that.find(url)[0].click();	
	        	} else {
	        		window.location = url;
	        	}
	        }
	      
	    }, 180));
	}

	function checkForCreateEdit(){
		var $bodyContent 	= $('.body-content'),
			exists 			= $bodyContent.children('.form-default').length > 0;

		if(!exists){
			$bodyContent.addClass('not-form')	
		}
	}

	function checkForErrorInputs(){
		var errors = $('.field-validation-error');

		$.each(errors, function(index, object){
			$(object).prev('input').addClass('is-invalid')
		})
	}

	function setDefaultScripts(){

		setDatePickers();
	}

	function setDatePickers(){

		// date
		var $date = $('.date-picker');
		$.each($date, function(index, object){
			var $this = $(object);

			$this.flatpickr({
				dateFormat: 'd/m/Y',
				locale: Portuguese
			});
		})

	    // range
	    var $datepickerrange = $('.date-picker-ranged');
	    $.each($datepickerrange, function(index, object){
	        var $this = $(object),
	            minDate = $this.data('mindate'),
	            maxDate = $this.data("maxdate"),
	            options = {
	                mode: 'range',
	                dateFormat: 'd/m/Y',
	                defaultDate: ['', ''],
	                locale: Portuguese
	            };

	        if(minDate){ options.minDate = minDate; }
	        if(maxDate){ options.maxDate = maxDate; }
	        $this.flatpickr(options);
	    });
	}

	function setHoverActiveColors(){
		var DOMSTYLES = window.getComputedStyle(document.documentElement),
			primaria = DOMSTYLES.getPropertyValue('--corPrimaria').trim(),
			secundaria = DOMSTYLES.getPropertyValue('--corSecundaria').trim();

		document.documentElement.style.setProperty('--corPrimariaHover', LightenDarkenColor(primaria, -10));
		document.documentElement.style.setProperty('--corPrimariaActive', LightenDarkenColor(primaria, -7));

		document.documentElement.style.setProperty('--corSecundariaHover', LightenDarkenColor(secundaria, -10));		
		document.documentElement.style.setProperty('--corSecundariaActive', LightenDarkenColor(secundaria, -22));
	}

})

$.fn.loading = function (status){
	var _this = $(this),
		isLoaded = status == "loaded",
		html 	 = $('#loadingTemplate').html(),
		isButton = _this.is('button'),
		isTable  = _this.is('table');

	if(isTable){
		_this.closest('.table-responsive').addClass('is-loading-table').append(html);
		if(isLoaded){
			_this.closest('.table-responsive').removeClass('is-loading-table').find('.spinner').remove();
		}
	} else {
		_this.addClass('is-loading-js').append(html);	
		if(isLoaded) {
			_this.removeClass('is-loading-js').find('.spinner').remove();
		}
	}

	return this;
}

$.fn.recalcularIndexes = function (childrenSelector, cssClass, beforeReplace, changeIds, callback, startIndex) {

    var _this = $(this),
        selector = _this.find(childrenSelector);

    if (startIndex == null) {
        startIndex = 0;
    }

    $.each(selector, function (index, object) {

        var inputs = $(object).find((cssClass || 'select, input'));
        $.each(inputs, function (i, o) {
            var _obj = $(o),
                name = _obj.attr('name'),
                regex = new RegExp(beforeReplace + "\\[\\\d\\]", "g");

            if (name) {
                var result = name.replace(regex, (beforeReplace + "[" + (index + startIndex) + "]"));
                _obj.attr('name', result);
                if (changeIds) {
                    _obj.attr('id', result);
                }
            }
        });

        if (callback) {
            console.log('callback!');
            callback(object, index);
        }
    });
}

$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  },
});

$.fn.serializeObject=function(){"use strict";var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};

String.prototype.toNumber = function(){
	return Number(this.replaceAll('.', '').replaceAll(',', '.')) || 0;
}

String.prototype.fakeHandlebars = function (obj, trimEverything) {
    var html = this.trim();
    var variables = this.match(/\{\{(.*?)\}\}/g);
    for (var i = variables.length - 1; i >= 0; i--) {
        var variable = variables[i].slice(2).slice(0, -2); // removendo brackets
        var replaced = (typeof obj[variable] === 'undefined' || obj[variable] == null) ? '' : obj[variable];
        if (trimEverything) {
            replaced = replaced.toString().trim();
        }
        html = html.replace(variables[i], replaced);
    }
    return html;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this, args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function LightenDarkenColor(col, amt) {
  
    var usePound = true;
  
    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }
 
    var num = parseInt(col,16);
 
    var r = (num >> 16) + amt;
 
    if (r > 255) r = 255;
    else if  (r < 0) r = 0;
 
    var b = ((num >> 8) & 0x00FF) + amt;
 
    if (b > 255) b = 255;
    else if  (b < 0) b = 0;
 
    var g = (num & 0x0000FF) + amt;
 
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
 
    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
  
}

function setTimezoneCookie() {
    var timezone_cookie = "timezoneoffset";

    // if the timezone cookie not exists create one.
    if (!$.cookie(timezone_cookie)) {
        // check if the browser supports cookie
        var test_cookie = 'test cookie';
        $.cookie(test_cookie, true);

        // browser supports cookie
        if ($.cookie(test_cookie)) {
            // delete the test cookie
            $.cookie(test_cookie, null);

            // create a new cookie
            $.cookie(timezone_cookie, new Date().getTimezoneOffset());

            // re-load the page
            location.reload();
        }
    }
    // if the current timezone and the one stored in cookie are different
    // then store the new timezone in the cookie and refresh the page.
    else {
        var storedOffset = parseInt($.cookie(timezone_cookie));
        var currentOffset = new Date().getTimezoneOffset();

        // user may have changed the timezone
        if (storedOffset !== currentOffset) {
            $.cookie(timezone_cookie, new Date().getTimezoneOffset());
            location.reload();
        }
    }
}