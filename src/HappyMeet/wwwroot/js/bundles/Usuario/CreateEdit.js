$(document).ready(function(){

	$('#tabela-permissoes').on('change', 'input', function(){
		var _this 		= $(this),
			_row 		= _this.closest('tr'),
			isChecked 	= _this.is(':checked'),
			isEscrita 	= $(this).hasClass('js-escrita');

		if(isEscrita){

			_row.find('.js-leitura').prop('checked', isChecked);
			
		} else {

			var $escrita 			= _row.find('.js-escrita'),
				isEscritaChecked	= $escrita.is(':checked');

			if(isEscritaChecked){
				_this.prop('checked', false)
			}

		}
	})

})