﻿$(document).ready(function () {

    var $dtEnvio = $('#dtEnvio');
    $dtEnvio.flatpickr({
        mode: 'range',
        dateFormat: 'd/m/Y',
        defaultDate: ['', ''],
        locale: Portuguese,
        onChange: function (selectedDates, dateStr, instance) {
            var hasFim = selectedDates.length == 2,
                hasInicio = selectedDates.length >= 1,
                dataInicio = hasInicio ? toDateTime(selectedDates[0]) : '',
                dataFim = hasFim ? toDateTime(selectedDates[1]) : '';

            $('#searchDtEnvioIni').val(dataInicio);
            $('#searchDtEnvioFim').val(dataFim);
        }
    });

    function toDateTime(dateObject) {
        var dia = checkTwoDigits(dateObject.getDate()),
            mes = checkTwoDigits(dateObject.getMonth() + 1),
            ano = dateObject.getFullYear(),
            hora = checkTwoDigits(dateObject.getHours()),
            min = checkTwoDigits(dateObject.getMinutes()),
            seconds = '00';

        return (mes + '/' + dia + '/' + ano + ' ' + hora + ':' + min + ':' + seconds);
    }

    function toDDMMYY(dateObject) {
        var dia = checkTwoDigits(dateObject.getDate()),
            mes = checkTwoDigits(dateObject.getMonth() + 1),
            ano = dateObject.getFullYear();

        return (dia + '/' + mes + '/' + ano);
    }

    function checkTwoDigits(number) {
        return number < 10 ? ('0' + number) : number
    }
});