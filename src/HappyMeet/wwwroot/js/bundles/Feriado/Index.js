﻿var currentYear = new Date().getFullYear(),
    selectedDayString = '',
    selectedDayObj = '',
    feriadoData = {
        uf: []
    },
    hasFeriado = false
$(document).ready(function () {


    $.fn.calendar.dates['pt'] = {
        days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
        daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
        daysMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
        months: ["Janeiro", "Feveiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        weekShort: 'S',
        weekStart: 0
    }

    $(".calendario").calendar({
        language: "pt",
        enableContextMenu: true,
        yearChanged: function (e) {
            currentYear = e.currentYear
            carregarCalendario(currentYear)
        },
    })
    carregarCalendario(currentYear)
    $('.calendario').clickDay(function (e) {
        selectedDayString = moment(e.date).format('DD/MM/YYYY')
        selectedDayObj = e.date
        if (e.events.length != 0) {
            getFeriadosById(e.events[0].id)
            hasFeriado = true
        } else {
            hasFeriado = false
            $('#event-modal').modal('show')
        }
    });
    $('#event-modal').on('shown.bs.modal', function () {
        getStates()
        if (hasFeriado) {
            $('.modal-title').html('Editar evento para: ' + selectedDayString)
            $('#desc').val(feriadoData.descricao)
            if (feriadoData.uf.length != 0) {
                $('#estadual').attr('checked', true)
                $('#estado').val(feriadoData.uf).change()
                $('.state-container').removeClass('hidden') 
            } else {
                $('#estadual').attr('checked', false)
                $('#estado').val('').change()
                $('.state-container').addClass('hidden')
            }
        } else {
            $('.modal-title').html('Marcar evento para: ' + selectedDayString)
            $('#desc').val('')
            $('.state-container').addClass('hidden')
            $('#estado').val('')
            $('#estado').trigger('change')
            $('#estadual').attr('checked', false)
            feriadoData = {}
        }
        //$('#estado').selectize()
        
        //$('#cidade').attr('disabled', true)
        //$('#cidade').html('<option value="" selected>Selecione o estado</option>')
    })
    $('#event-modal').on('hide.bs.modal', function () {
        $('#estado').val('')
        $('#estado').trigger('change')
        $('#desc').val('')
        $('#estadual').attr('checked', false)
    })
    $('#estado').on('change', function () {
        //$.ajax({
        //    url: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/' + $(this).val() + '/municipios',
        //    method: 'GET',
        //    success: function (cidadesJson) {
        //        options = '<option value="" selected>Selecione</option>'
        //        $.each(cidadesJson, function (i, e) {
        //            options += '<option value="' + e.nome + '">' + e.nome + '</option>'
        //        })
        //        $('#cidade').html(options)
        //        $('#cidade').removeAttr('disabled')
        //    }
        //})
    })
    $('#save-event').on('click', function () {
        saveFeriado()
    })
    $('#remove').on('click', function () {
        removeFeriado(feriadoData.id)
    })
    $('body').on('change', '#estadual', function (e) {
        e.preventDefault()
        if ($(this).is(':checked')) {
            $('.state-container').removeClass('hidden')
        } else {
            $('.state-container').addClass('hidden')
        }
    })
});

function carregarCalendario(ano) {
    $.ajax({
        url: getFeriadosByAnoUrl,
        type: 'GET',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: { ano },
        success: function (result) {
            $.each(result.feriados, function (i, o) {
                o.startDate = new Date(o.startDate);
                o.endDate = new Date(o.endDate);
            });
            $('.calendario').data('calendar').setDataSource(result.feriados);
        }
    });
}
function saveFeriado() {
    $.ajax({
        url: saveFeriadoUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        type: 'POST',
        dataType: "json",
        data: { id: feriadoData.id, descricao: $('#desc').val(), data: selectedDayObj.toJSON(), uf: $('#estado').val(), cidade: $('#cidade').val() },
        success: function (saveReturn) {
            swal(
                'Sucesso!',
                saveReturn.message,
                'success'
            )
            $('#event-modal').modal('hide')
            carregarCalendario(currentYear)
        }
    })

}

function getFeriadosById(id) {
    $.ajax({
        url: getFeriadosByIdUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        type: 'POST',
        dataType: "json",
        data: { id: id },
        success: function (getFeriadoData) {
            feriadoData = getFeriadoData
            feriadoData.id = id
            $('#event-modal').modal('show')
        }
    })

}

function removeFeriado(id) {
    $.ajax({
        url: removeFeriadoUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        type: 'POST',
        dataType: "json",
        data: {id: id},
        success: function (removeReturn) {
            if (removeReturn.success) {
                $('#event-modal').modal('hide')
                swal(
                    'Sucesso!',
                    'Feriado removido!',
                    'success'
                )
                carregarCalendario(currentYear)
            }
        }
    })

}

function getStates() {
    $('#estado').html('')
    $.ajax({
        url: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados',
        method: 'GET',
        async : false,
        success: function (estadosJson) {
            estadosJson.sort(function (a, b) {
                if (a.sigla < b.sigla) return -1;
                if (a.sigla > b.sigla) return 1;
                return 0;
            });
            estadosJson.forEach(function (estado) {
                var opt = document.createElement('option');
                opt.value = estado.sigla
                opt.setAttribute("data-uf", estado.sigla)
                opt.innerHTML = estado.nome;
                $('#estado').append(opt)
            });
        }
    })
}
