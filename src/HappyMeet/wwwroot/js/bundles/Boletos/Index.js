﻿$(document).ready(function () {

    var $TABLE = $('.table-boletos'),
        $THEADINPUT = $TABLE.find('thead input'),
        $TOGGLELIST = $('.js-toggle-lista'),
        $BOLETOSHEAD = $('#boletos-header'),
        $BOLETOSCONT = $('#boletos-content'),
        numeroMask = new StringMask('#.##0,00', { reverse: true }),
        aindaNaoCansouDaAnimacao = true;

    $BOLETOSHEAD.stick_in_parent({
        offset_top: $BOLETOSHEAD.offset().top
    });

    $('body').on('change', 'thead input[type=checkbox]', function () {
        var isChecked = $(this).is(':checked'),
            qtdInputs = $TABLE.find('tbody').find('input').length;

        $TABLE.find('tbody input').prop('checked', isChecked)
        $TOGGLELIST.attr('disabled', !(isChecked && qtdInputs > 0));
        calcularTotal();
    })

    $('body').on('change', 'tbody input', function () {
        var $parent = $(this).closest('tbody'),
            qtdChecked = $parent.find('input:checked').length,
            qtdInputs = $parent.children('tr').length;

        $THEADINPUT.prop('checked', (qtdChecked === qtdInputs));
        $TOGGLELIST.attr('disabled', !(qtdChecked > 0));
        calcularTotal();
    });

    var $Emissao = $('#Emissao');
    $Emissao.flatpickr({
        mode: 'range',
        dateFormat: 'd/m/Y',
        defaultDate: ['', ''],
        locale: Portuguese,
        onChange: function (selectedDates, dateStr, instance) {
            var hasFim = selectedDates.length == 2,
                hasInicio = selectedDates.length >= 1,
                dataInicio = hasInicio ? toDateTime(selectedDates[0]) : '',
                dataFim = hasFim ? toDateTime(selectedDates[1]) : '';

            $('#searchEmissaoIni').val(dataInicio);
            $('#searchEmissaoFim').val(dataFim);
        }
    });

    var $Vencimento = $('#Vencimento');
    $Vencimento.flatpickr({
        mode: 'range',
        dateFormat: 'd/m/Y',
        defaultDate: ['', ''],
        locale: Portuguese,
        onChange: function (selectedDates, dateStr, instance) {
            var hasFim = selectedDates.length == 2,
                hasInicio = selectedDates.length >= 1,
                dataInicio = hasInicio ? toDateTime(selectedDates[0]) : '',
                dataFim = hasFim ? toDateTime(selectedDates[1]) : '';

            $('#searchVencimentoIni').val(dataInicio);
            $('#searchVencimentoFim').val(dataFim);
        }
    });

    var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
        '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

    $('#Emails').selectize({
        persist: false,
        maxItems: null,
        valueField: 'email',
        labelField: 'name',
        searchField: ['name', 'email'],
        options: [],
        render: {
            item: function (item, escape) {
                return '<div>' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                    '</div>';
            },
            option: function (item, escape) {
                var label = item.name || item.email;
                var caption = item.name ? item.email : null;
                return '<div>' +
                    '<span class="label">' + escape(label) + '</span>' +
                    (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
            }
        },
        createFilter: function (input) {
            var match, regex;

            // email@address.com
            regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[0]);

            // name <email@address.com>
            regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[2]);

            return false;
        },
        create: function (input) {
            if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                return { email: input };
            }
            var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
            if (match) {
                return {
                    email: match[2],
                    name: $.trim(match[1])
                };
            }
            alert('Invalid email address.');
            return false;
        }
    });

    $("#bt-download").on('click', download);
    $("#bt-send").on('click', sendEmail);

    setAtrasoStyling();

    function toDateTime(dateObject) {
        var dia = checkTwoDigits(dateObject.getDate()),
            mes = checkTwoDigits(dateObject.getMonth() + 1),
            ano = dateObject.getFullYear(),
            hora = checkTwoDigits(dateObject.getHours()),
            min = checkTwoDigits(dateObject.getMinutes()),
            seconds = '00';

        return (mes + '/' + dia + '/' + ano + ' ' + hora + ':' + min + ':' + seconds);
    }

    function toDDMMYY(dateObject) {
        var dia = checkTwoDigits(dateObject.getDate()),
            mes = checkTwoDigits(dateObject.getMonth() + 1),
            ano = dateObject.getFullYear();

        return (dia + '/' + mes + '/' + ano);
    }

    function checkTwoDigits(number) {
        return number < 10 ? ('0' + number) : number
    }

    function calcularTotal() {
        var qtdInputs = $BOLETOSCONT.find('.table-boletos').find('input[type="checkbox"]:checked').length,
            valores = $BOLETOSCONT.find('input:checked').toArray(),
            total = valores.reduce(calculaTotal, 0),
            acrescimos = valores.reduce(calculaAcrescimos, 0),
            totalGeral = 0;

        function calculaTotal(acumulador, currentObject) {
            var valor = $(currentObject).closest('tr').find('.js-valor').text().toNumber();
            return (acumulador + valor);
        }

        function calculaAcrescimos(acumulador, currentObject) {
            var $ROW = $(currentObject).closest('tr'),
                total = $ROW.find('.js-valor').text().toNumber(),
                totalAtualizado = $ROW.find('.js-valor-atualizado').text().toNumber();

            return (acumulador + (totalAtualizado - total));
        }

        acrescimos = acrescimos.toFixed(2);
        total = total.toFixed(2);
        totalGeral = (Number(total) + Number(acrescimos)).toFixed(2);

        $BOLETOSHEAD.find('#boletos-details').toggleClass('hidden', qtdInputs === 0);
        $BOLETOSHEAD.find('.js-qtd-boletos').text(qtdInputs);
        $BOLETOSHEAD.find('.js-total').text(total).trigger('input');
        $BOLETOSHEAD.find('.js-acrescimos').text(acrescimos).trigger('input');
        $BOLETOSHEAD.find('.js-total-atualizado').text(totalGeral).trigger('input');

        console.log('Total', total);
        console.log('Acrescimos', acrescimos);
        console.log('Atualizado', total + acrescimos);

        if (qtdInputs > 0 && aindaNaoCansouDaAnimacao) {
            $('#bt-download').animateCss('shake');
            setTimeout(function () {
                $('#bt-emails').animateCss('shake');
            }, 10);
            aindaNaoCansouDaAnimacao = false;
        }

    }

    function setAtrasoStyling() {
        var $atraso = $('#atraso'),
            valor = $atraso.find('.js-total-atraso').text().toNumber();

        $atraso.toggleClass('is-danger', valor > 0);
    }
});




function download() {
    var boletosInline = '';
    $('#boletos-content .table-boletos').find('input[type="checkbox"]:checked').each(function () {
        boletosInline += $(this).data('boleto') + ',';
    });
    boletosInline = boletosInline.replace(/,\s*$/, "");

    window.location = downloadUrl + '/idsInline?=' + boletosInline;
}

function sendEmail() {
    var boletos = [];
    $('#boletos-content .table-boletos').find('input[type="checkbox"]:checked').each(function () {
        boletos.push($(this).data('boleto'));
    });

    $.ajax({
        url: sendEmailUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        type: 'POST',
        dataType: "json",
        data: { idBoletos: boletos, emails: $("#Emails").val() },
        success: function (result) {
            if (result.success) {
                swal(
                    'Sucesso!',
                    'Os boletos foram enviados por email!',
                    'success'
                )
            } else {
                swal(
                    'Atenção!',
                    result.alertas,
                    'warning'
                )
            }
        }
    })
}