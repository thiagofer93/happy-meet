﻿$(document).ready(function(){

	$('#UserName').on('keypress', function(e){
		var _this 		=	 $(this),
			value 		= e.target.value,
			qtdChars 	= value.length;

		if(qtdChars >= 2){

			var isNumbers 	= Boolean( isFinite(value[0]) && isFinite(value[1]) );

			if(isNumbers){
				var CpfCnpjMaskBehavior = function (val) {
						return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
					},
					cpfCnpjpOptions = {
						onKeyPress: function(val, e, field, options) {
							field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
						}
					};
				_this.mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);
			} else {
				_this.unmask();
			}
		} else {

			_this.unmask();
		}
	});

	$('.form-signin').on('submit', function(){
		$('#logar').loading('loading');
	})
	
})