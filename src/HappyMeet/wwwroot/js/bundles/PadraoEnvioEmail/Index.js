﻿$(document).ready(function () {
    $('.js-adicionar-email').on('click', function () {
        var $TEMPLATE = $('#templateEnvios').html(),
            $CONTAINER = $(this).closest('section'),
            $TIPOEMAIL = $(this).data("tipo-email"),
            html = $TEMPLATE.fakeHandlebars({ TipoEmail: $TIPOEMAIL });

        $CONTAINER.find('.lista-emails').append(html);

        $.applyDataMask();

        toggleEmptyContainerDiv($CONTAINER, '.lista-emails > li');

        $('.lista-envios-email').recalcularIndexes('li', null, 'Model', true, null, 2);
        return false;
    });

    $('section').on('click', '.js-delete-email', function () {
        var $CONTAINER = $(this).closest('section');

        $(this).closest('li').remove();
        toggleEmptyContainerDiv($CONTAINER, '.lista-emails > li');
        $('.lista-envios-email').recalcularIndexes('.lista-emails > li', null, 'Model', true, null, 2);
        return false;
    });
});
function toggleEmptyContainerDiv(CONTAINER, selector) {
    var quantidadeItens = CONTAINER.find(selector).length;
    CONTAINER.find('.empty-container').toggleClass('hidden', Boolean(quantidadeItens))
}