﻿$(document).ready(function () {

    var TEMPLATE = $('#templateFile').html(),
        $FILE = $('#file');

    $('.js-selecionar').on('click', function (e) {
        $FILE.trigger('click');
        return false;
    })

    $FILE.on('change', function (e) {
        var filename = e.target.value.split(/(\\|\/)/g).pop();
        $FILE.next().html(filename);
        $('.lista-arquivos').html(TEMPLATE.fakeHandlebars({ filename }));
    });
});