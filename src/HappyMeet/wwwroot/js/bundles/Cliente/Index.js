﻿$(document).ready(function(){

	var $CNPJFIELD = $('#CNPJ');	
	var CpfCnpjMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
		},
		cpfCnpjpOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
			}
		};
	$CNPJFIELD.mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);

	$('body').on('click', '.js-excluir', function(){

		swal({
			title: 'Você tem certeza?',
			text: "Você não poderá reverter isso!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remover!',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				swal(
					'Deletado!',
					'Este cliente foi deletado.',
					'success'
				)
			}
		});

	});
	
})