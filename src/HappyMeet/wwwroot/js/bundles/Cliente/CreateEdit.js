﻿$(document).ready(function () {

    checkIfEdit();
    setCpfCnpjFields();

    var $CNPJFIELD = $('#CpfCnpj');
    var CpfCnpjMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
        },
        cpfCnpjpOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
            }
        };
    $CNPJFIELD.mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);

    $('.js-adicionar-email').on('click', function () {
        var $TEMPLATE = $('#templateEnvios').html(),
            $CONTAINER = $(this).closest('section'),
            $TIPOEMAIL = $(this).data("tipo-email"),
            html = $TEMPLATE.fakeHandlebars({ TipoEmail: $TIPOEMAIL });

        $CONTAINER.find('.lista-emails').append(html);

        $.applyDataMask();

        toggleEmptyContainerDiv($CONTAINER, '.lista-emails > li');

        $('.lista-envios-email').recalcularIndexes('li', null, 'EmailsLembrete', true);
        return false;
    });

    $('section').on('click', '.js-delete-email', function () {
        var $CONTAINER = $(this).closest('section');

        $(this).closest('li').remove();
        toggleEmptyContainerDiv($CONTAINER, '.lista-emails > li');
        $('.lista-envios-email').recalcularIndexes('li', null, 'EmailsLembrete', true);
        return false;
    });

    $('.js-adicionar-cnpj').on('click', function () {
        var $TEMPLATE = $('#templateCnpj').html(),
            $UL = $('#lista-cnpjs'),
            $CONTAINER = $UL.closest('.lista-cnpjs-container');

        $UL.append($TEMPLATE.fakeHandlebars({ cnpj: '', nome: '' }));
        toggleEmptyContainerDiv($CONTAINER, '#lista-cnpjs > li');
        $('#lista-cnpjs').recalcularIndexes('> li', null, 'EmpresasAgrupadas', true);
        setCpfCnpjFields();
        $.applyDataMask();
        return false;
    });

    $('#lista-cnpjs').on('click', '.js-remover-cnpj', function () {
        var $CONTAINER = $(this).closest('.lista-cnpjs-container');

        $(this).closest('li').remove();
        toggleEmptyContainerDiv($CONTAINER, '#lista-cnpjs > li');
        $('#lista-cnpjs').recalcularIndexes('> li', null, 'EmpresasAgrupadas', true);
        return false;
    })

    var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
        '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

    $('#Emails').selectize({
        persist: false,
        maxItems: null,
        valueField: 'email',
        labelField: 'name',
        searchField: ['name', 'email'],
        options: [],
        render: {
            item: function (item, escape) {
                return '<div>' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                    '</div>';
            },
            option: function (item, escape) {
                var label = item.name || item.email;
                var caption = item.name ? item.email : null;
                return '<div>' +
                    '<span class="label">' + escape(label) + '</span>' +
                    (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
            }
        },
        createFilter: function (input) {
            var match, regex;

            // email@address.com
            regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[0]);

            // name <email@address.com>
            regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[2]);

            return false;
        },
        create: function (input) {
            if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                return { email: input };
            }
            var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
            if (match) {
                return {
                    email: match[2],
                    name: $.trim(match[1])
                };
            }
            alert('Invalid email address.');
            return false;
        }
    });

    function toggleEmptyContainerDiv(CONTAINER, selector) {
        var quantidadeItens = CONTAINER.find(selector).length;
        CONTAINER.find('.empty-container').toggleClass('hidden', Boolean(quantidadeItens))
    }

    $("#CpfCnpj").blur(function () {
        if ($("#CpfCnpj").val().length == 18) {
            $.ajax({
                url: getClientesRelacionadosUrl,
                type: 'GET',
                dataType: "json",
                data: { cnpj: $("#CpfCnpj").val() },
                success: function (result) {
                    if (result.success) {
                        $("#NomeCliente").val(result.nomeCliente);
                        var html = '',
                            $template = $('#templateCnpj').html(),
                            $UL = $('#lista-cnpjs'),
                            $CONTAINER = $UL.closest('.lista-cnpjs-container');

                        for (var i = 0; i < result.relacionados.length; i++) {
                            var CNPJ            = result.relacionados[i].cnpj,
                                alreadyExists   = $.grep($('.js-cpf-cnpj'), function(o){ return $(o).val() == CNPJ }).length > 0;

                            if(!alreadyExists){
                                html += $template.fakeHandlebars(result.relacionados[i]);    
                            }                            
                        }                        

                        $UL.append(html);
                        toggleEmptyContainerDiv($CONTAINER, '#lista-cnpjs > li');
                        $('#lista-cnpjs').recalcularIndexes('> li', null, 'EmpresasAgrupadas', true);
                        setCpfCnpjFields();
                        $.applyDataMask();

                    }
                }
            });
        }
    });

    function checkIfEdit(){
        var isEdit = $('#Id').val() == "0";
        
        if(isEdit){
            $('#CpfCnpj').attr('readonly');
        }
    }

    function setCpfCnpjFields(){
        var $CNPJFIELD = $('.js-cpf-cnpj');
        var CpfCnpjMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
            },
            cpfCnpjpOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
                }
            };
        $CNPJFIELD.mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);        
    }
})