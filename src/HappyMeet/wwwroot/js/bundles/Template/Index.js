﻿$(document).ready(function(){

	$('body').on('click', '.js-excluir', function(){

		swal({
			title: 'Você tem certeza?',
			text: "Você não poderá reverter isso!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remover!',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				swal(
					'Deletado!',
					'Este cliente foi deletado.',
					'success'
				)
			}
		});

	});
	
})