﻿$(document).ready(function(){

	$.trumbowyg.svgPath = '/lib/trumbowyg/dist/ui/icons.svg';

	var $MSG = $('#Mensagem'),
		VARIAVEIS 	= JSON.parse(variaveisEmail),
		dropdown 	= [];
		varsObject  = {};

	$.trumbowyg.langs.en.base64 = 'Inserir imagem';
	$.trumbowyg.langs.pt_br.insertImage = 'Inserir imagem (URL)';

	$.each(VARIAVEIS, function(index, object){
		var objeto = {
			fn: function(varName){
				$MSG.trumbowyg('execCmd', {
					cmd: 'insertText',
					param: ('{{' + varName + '}}'),
					forceCss: false,
				});
			},
            title: object.Item2, 
            ico: 'blockquote'
		}
		varsObject[object.Item1] = objeto;
		dropdown.push(object.Item1);
	});

	var variaveisItem = {
	        fn: 'functionName',
	        tag: 'tagName',
	        title: 'Variáveis para email',
	        dropdown: dropdown,
	        text: 'Variáveis',
	        isSupported: function () { return true; },
	        key: 'K',
	        param: '' ,
	        forceCSS: false,
	        class: '',
	        hasIcon: false
	    },
    	imageItem = {
    		title: 'Inserir imagem',
            dropdown: ['base64', 'insertImage'],
            ico: 'insertImage'
        };

	$MSG.trumbowyg({
		lang: 'pt_br',
		btnsDef: $.extend(varsObject, { 'variaveis': variaveisItem, 'image': imageItem }),
	    btns: [
	        ['viewHTML'],
	        ['undo', 'redo'], // Only supported in Blink browsers
	        ['formatting'],
	        ['strong', 'em', 'del'],
	        ['superscript', 'subscript'],
	        ['link'],
	        ['image'],
	        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
	        ['unorderedList', 'orderedList'],
	        ['horizontalRule'],
	        ['removeformat'],
	        ['variaveis'],
	        ['fullscreen']
	    ]
	});
	
})