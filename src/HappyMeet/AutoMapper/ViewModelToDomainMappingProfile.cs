﻿using System;
using System.Collections.Generic;
using AutoMapper;
using HappyMeet.Domain.Entities;
using HappyMeet.Models;

namespace HappyMeet.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<BancoViewModel, Banco>();
            CreateMap<ConfiguracaoViewModel, Configuracao>();
            CreateMap<BoletoViewModel, Boleto>();
        }
    }
}