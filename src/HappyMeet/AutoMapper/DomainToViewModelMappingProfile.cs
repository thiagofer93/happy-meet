﻿using AutoMapper;
using HappyMeet.Domain.Entities;
using HappyMeet.Models;

namespace HappyMeet.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        public DomainToViewModelMappingProfile()
        {
            CreateMap<Banco, BancoViewModel>();
            //CreateMap<ApplicationUser, UsuarioViewModel>()
            //    .ForMember(x=>x.Telas, opt => opt.Ignore());
            CreateMap<Configuracao, ConfiguracaoViewModel>();
        }
    }
}