﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Nito.AsyncEx;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Requests;
using HappyMeet.Domain.Services;
using HappyMeet.Extensions;
using HappyMeet.Handlers;
using HappyMeet.Repository.Handlers;
using HappyMeet.Utils;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace HappyMeet
{
    public class CompositionRoot : IPackage
    {
        public void RegisterServices(Container container)
        {
            container.RegisterMediatR();
            container.RegisterHandler<PersistUserHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindBancosHandler>(Lifestyle.Scoped);
            container.RegisterHandler<PersistBancoHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindUsersHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindUserByIdHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindRolesAndClaimsByUserHandler>(Lifestyle.Scoped);
            container.RegisterHandler<SendEmailHandler>(Lifestyle.Scoped);
            container.RegisterHandler<ResetPasswordHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindConfiguracaoHandler>(Lifestyle.Scoped);
            container.RegisterHandler<PersistConfiguracaoHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindBoletosHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindBoletoCountHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindUserCountHandler>(Lifestyle.Scoped);
            container.RegisterHandler<FindBancoCountHandler>(Lifestyle.Scoped);

            container.Register<AppConfig>(Lifestyle.Singleton);
        }
    }
}