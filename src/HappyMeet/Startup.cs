﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HappyMeet.Repository;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using System;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Globalization;
using System.Net;
using HappyMeet.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using HappyMeet.Domain.Util;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using HappyMeet.Domain.Services;
using HappyMeet.Middleware;
using Swashbuckle.AspNetCore.Swagger;
using UrlHelper = Microsoft.AspNetCore.Mvc.Routing.UrlHelper;
using MediatR;

namespace HappyMeet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private readonly Container _container = new Container();

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                 .RequireAuthenticatedUser()
                                 .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
                config.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });
            //.AddFluentValidation(options => options.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddAutoMapper();

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<PostgresContext>(
                    options => options.UseNpgsql(
                        Configuration.GetConnectionString("Postgres"),
                        npgOptions => npgOptions.MigrationsAssembly("HappyMeet.Repository")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            options.User.AllowedUserNameCharacters += "./-")
                    .AddEntityFrameworkStores<PostgresContext>()
                    .AddDefaultTokenProviders()
                    .AddErrorDescriber<PortugueseLocalizedIdentityDescriber>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                // If the LoginPath isn't set, ASP.NET Core defaults 
                // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
            });
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new Info() {Title = "API para carga de boletos", Version = "v1"});
                options.DescribeAllEnumsAsStrings();
                options.OperationFilter<SetBodyParameters>();
            });

            services.AddAuthorization(options =>
            {
                foreach (var claim in ClaimConstants.CLAIMS)
                {
                    options.AddPolicy(claim, policy => policy.RequireClaim(ClaimsCustomType.PERMISSAO, claim));
                }
            });

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>(factory =>
            {
                var actionContext = factory.GetService<IActionContextAccessor>()
                                           .ActionContext;
                return new UrlHelper(actionContext);
            });

            IntegrateSimpleInjector(services);
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, CustomClaimsPrincipalFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            CultureInfo ci = new CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);

            InitializeContainer(app);
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseWhen(x => x.Request.Path.StartsWithSegments("/api", StringComparison.OrdinalIgnoreCase),
                builder =>
                {
                    builder.UseMiddleware<ApiOriginMiddleware>();
                    builder.UseMiddleware<AuthenticationMiddleware>();
                });
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API para carga de boletos. V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IControllerActivator>(
                new SimpleInjectorControllerActivator(_container));
            services.AddSingleton<IViewComponentActivator>(
                new SimpleInjectorViewComponentActivator(_container));

            services.EnableSimpleInjectorCrossWiring(_container);
            services.UseSimpleInjectorAspNetRequestScoping(_container);
            services.AddSingleton<AppConfig>(provider => _container.GetInstance<AppConfig>());
        }

        private void InitializeContainer(IApplicationBuilder app)
        {
            _container.RegisterMvcControllers(app);
            _container.RegisterMvcViewComponents(app);
            //_container.CrossWire<ILoggerFactory>(app);
            //_container.CrossWire<IConfiguration>(app);
            _container.AutoCrossWireAspNetComponents(app);
            _container.RegisterPackages(new[] { typeof(CompositionRoot).Assembly });
        }
    }
}
