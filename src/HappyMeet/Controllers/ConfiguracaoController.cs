﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Requests;
using HappyMeet.Domain.Util;
using HappyMeet.Models;
using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace HappyMeet.Controllers
{
    [Authorize(Roles = RoleConstants.ADMIN)]
    public class ConfiguracaoController : BaseController
    {
        public ConfiguracaoController(IMediator mediator) : base(mediator)
        {

        }
        // GET
        public async Task<IActionResult> Index()
        {
            var config = await Mediator.Send(new FindConfiguracaoRequest());

            var model = new ConfiguracaoViewModel()
            {
                CorPrimaria = "#007bff",
                CorSecundaria = "#6c757d",
                CorFonte = "#ffffff"
            };

            if (config.Value != null)
            {
                model = Mapper.Map<Configuracao, ConfiguracaoViewModel>(config.Value);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(ConfiguracaoViewModel model, IFormFile logo, IFormFile fundo)
        {
            if (ModelState.IsValid)
            {
                Configuracao config;

                string nomeLogo;
                string nomeFundo;
                var imagemLogo = FileToByteArray(logo, out nomeLogo);
                var imagemFundo = FileToByteArray(fundo, out nomeFundo);

                if (model.Id == 0)
                {
                    config = Mapper.Map<ConfiguracaoViewModel, Configuracao>(model);
                }
                else
                {
                    var find = await Mediator.Send(new FindConfiguracaoRequest());
                    config = find.Value;

                    if (String.IsNullOrEmpty(model.EmailPassword)) model.EmailPassword = config.EmailPassword;

                    if (config.ImagemLogo != null && !imagemLogo.Any())
                    {
                        imagemLogo = (byte[])config.ImagemLogo.Clone();
                    }
                    if (config.ImagemFundo != null && !imagemFundo.Any())
                    {
                        imagemFundo = (byte[])config.ImagemFundo.Clone();
                    }
                    Mapper.Map(model, config);
                }

                config.ImagemLogo = imagemLogo;
                config.ImagemFundo = imagemFundo;

                if (!String.IsNullOrEmpty(nomeLogo)) config.NomeLogo = nomeLogo;
                if (!String.IsNullOrEmpty(nomeFundo)) config.NomeFundo = nomeFundo;

                var response = await Mediator.Send(new PersistConfiguracaoRequest(config));

                if (response)
                {
                    Success(response.Value);
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
    }
}