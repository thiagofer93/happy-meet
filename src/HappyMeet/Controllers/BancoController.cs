﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Requests;
using HappyMeet.Domain.Util;
using HappyMeet.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using X.PagedList;
using HappyMeet.Domain.Extensions;

namespace HappyMeet.Controllers
{
    [Authorize(Policy = ClaimConstants.BANCO_LEITURA)]
    public class BancoController : BaseController
    {
        public BancoController(IMediator mediator) : base(mediator)
        {

        }

        public async Task<IActionResult> Index(int searchCodBanco, string searchNome, string sortProperty, string sortOrder, int pagina)
        {
            var searchExpression = GetSearchExpression(searchCodBanco, searchNome);
            var response = await Mediator.Send(new FindBancosRequest(searchExpression, sortProperty, sortOrder, pagina, RegistrosPorPagina));

            var model = new BancoPresentationViewModel();

            model.Bancos = response.Value.ToArray();
            model.RegistrosPorPagina = RegistrosPorPagina;
            model.CurrentPage = pagina;
            model.TotalCount = await Mediator.Send(new FindBancoCountRequest(searchExpression));
            model.BancosPaged = new StaticPagedList<Banco>(model.Bancos, pagina, RegistrosPorPagina, model.TotalCount);

            return View(model);
        }

        public async Task<IActionResult> CreateEdit(int? id)
        {
            BancoViewModel model;
            if (id.HasValue)
            {
                var response = await Mediator.Send(new FindBancosRequest(x => x.Id.Equals(id.Value)));
                model = Mapper.Map<Banco, BancoViewModel>(response.Value.FirstOrDefault());
            }
            else
            {
                model = new BancoViewModel
                {
                    Ativo = true
                };
            }

            return View(model);
        }

        [Authorize(Policy = ClaimConstants.BANCO_ESCRITA)]
        [HttpPost]
        public async Task<IActionResult> CreateEdit(BancoViewModel model, IFormFile file)
        {

            if (ModelState.IsValid)
            {
                Banco banco;

                string nomeArquivo;
                byte[] arquivo = FileToByteArray(file, out nomeArquivo);

                if (model.Id == 0)
                {
                    banco = Mapper.Map<BancoViewModel, Banco>(model);
                }
                else
                {
                    var find = await Mediator.Send(new FindBancosRequest(x => x.Id.Equals(model.Id)));
                    banco = find.Value.FirstOrDefault();

                    if (banco.Arquivo != null && !arquivo.Any())
                    {
                        arquivo = (byte[])banco.Arquivo.Clone();
                    }

                    Mapper.Map(model, banco);
                }

                if(!String.IsNullOrEmpty(nomeArquivo)) banco.NomeArquivo = nomeArquivo;

                banco.Arquivo = arquivo;

                var response = await Mediator.Send(new PersistBancoRequest(banco));

                if (response)
                {
                    Success(response.Value);
                    return RedirectToAction("Index");
                }
                AddError(response.Error);
            }

            return View(model);
        }


        public async Task<ActionResult> DownloadArquivo(int id)
        {
            var response = await Mediator.Send(new FindBancosRequest(x => x.Id.Equals(id)));

            var banco = response.Value.FirstOrDefault();

            if (banco.Arquivo == null || banco.Arquivo.Length == 0)
            {
                return Json(new { success = false });
            }

            //MemoryStream fileStream = new MemoryStream(banco.Arquivo);
            return new FileContentResult(banco.Arquivo, "application/octet-stream")
            {
                FileDownloadName = banco.NomeArquivo
            };
        }

        private Expression<Func<Banco, bool>> GetSearchExpression(int searchCodBanco, string searchNome)
        {
            Expression<Func<Banco, bool>> exp = x => true;

            if (searchCodBanco > 0)
            {
                exp = exp.And(x => x.Codigo.Equals(searchCodBanco));
            }

            if (!String.IsNullOrEmpty(searchNome))
            {
                exp = exp.And(x => x.NomeBanco.ToLower().Contains(searchNome.ToLower()));
            }

            return exp;
        }
    }
}
