﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using HappyMeet.Domain.Requests;
using HappyMeet.Domain.Util;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using HappyMeet.Models;
using System;
using AutoMapper;
using System.IO;
using System.IO.Compression;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using System.Linq.Expressions;
using HappyMeet.Domain.Extensions;
using X.PagedList;
using HappyMeet.Domain.Enums;

namespace HappyMeet.Controllers
{
    [Authorize(Policy = ClaimConstants.BOLETO_LEITURA)]
    public class BoletosController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public BoletosController(IMediator mediator, UserManager<ApplicationUser> userManager) : base(mediator)
        {
            _userManager = userManager;
        }

        // GET
        public async Task<IActionResult> Index(string searchCnpj, string searchNome, string searchNf, string searchNumDocum, DateTime? searchEmissaoIni, DateTime? searchEmissaoFim, DateTime? searchVencimentoIni, DateTime? searchVencimentoFim, decimal searchValor, string sortProperty, string sortOrder, int pagina)
        {
            BoletoViewModel model = new BoletoViewModel();
            return View(model);
        }

        private Expression<Func<Boleto, bool>> GetSearchExpression(string searchCnpj, string searchNome, string searchNf, string searchNumDocum, DateTime? searchEmissaoIni, DateTime? searchEmissaoFim, DateTime? searchVencimentoIni, DateTime? searchVencimentoFim, decimal searchValor)
        {
            Expression<Func<Boleto, bool>> exp = x => true;

            if (!String.IsNullOrEmpty(searchCnpj))
            {
                exp = exp.And(x => x.CpfCnpjPagador.Contains(searchCnpj.Trim()));
            }

            if (!String.IsNullOrEmpty(searchNome))
            {
                exp = exp.And(x => x.NomePagador.ToLower().Contains(searchNome.ToLower()));
            }

            if (!String.IsNullOrEmpty(searchNf))
            {
                exp = exp.And(x => x.NotaFiscal.Contains(searchNf));
            }

            if (!String.IsNullOrEmpty(searchNumDocum))
            {
                exp = exp.And(x => x.NumDocumento.Contains(searchNumDocum));
            }

            if (searchEmissaoIni.HasValue)
            {
                exp = exp.And(x => x.DtEmissao >= searchEmissaoIni.Value.Date);
            }

            if (searchEmissaoFim.HasValue)
            {
                exp = exp.And(x => x.DtEmissao <= searchEmissaoFim.Value.ToEndOfTheDay());
            }

            if (searchVencimentoIni.HasValue)
            {
                exp = exp.And(x => x.DtVencimento >= searchVencimentoIni.Value.Date);
            }

            if (searchVencimentoFim.HasValue)
            {
                exp = exp.And(x => x.DtVencimento <= searchVencimentoFim.Value.ToEndOfTheDay());
            }

            if (searchValor > 0)
            {
                exp = exp.And(x => x.Valor == searchValor);
            }

            return exp;
        }
    }

    class InfoConsulta
    {
        public string[] CpfsCnpjs { get; set; }
        public string CodRepresentante { get; set; }
        public List<string> Emails { get; set; }
    }
}