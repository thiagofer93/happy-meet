﻿using System.Diagnostics;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using HappyMeet.Domain.Util;
using HappyMeet.Models;
using HappyMeet.Repository;
using System.Linq;

namespace HappyMeet.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(IMediator mediator, PostgresContext context) : base(mediator)
        {
        }

        public IActionResult Index()
        {
            return RedirectToAction("Index", "Boletos");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
