﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using HappyMeet.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HappyMeet.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Mvc.Filters;

namespace HappyMeet.Controllers
{
    public class BaseController : Controller
    {
        protected IMediator Mediator { get; }

        public BaseController(IMediator mediator)
        {
            Mediator = mediator;
        }

        protected void AddError(string errorMessage)
        {
            ModelState.AddModelError("", errorMessage);
        }

        protected void AddError(string[] errors)
        {
            foreach (var e in errors)
            {
                AddError(e);
            }
        }


        public void Success(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }

        public void Information(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Information, message, dismissable);
        }

        public void Warning(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }

        public void Danger(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }

        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? TempData.Get<List<Alert>>(Alert.TempDataKey)
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData.Put(Alert.TempDataKey, alerts);
        }

        protected byte[] FileToByteArray(IFormFile arquivo, out string fileName)
        {
            byte[] bytes = new byte[] { };
            fileName = "";

            if (arquivo != null && arquivo.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    arquivo.CopyTo(ms);
                    bytes = ms.ToArray();
                }

                fileName = arquivo.FileName;
            }

            return bytes;
        }

        protected virtual int RegistrosPorPagina { get { return 100; } }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (HttpContext.Request.Cookies.Keys.Contains("timezoneoffset"))
            {
                DateTimeExtension.ClientTimezoneOffset = HttpContext.Request.Cookies["timezoneoffset"];
            }
            base.OnActionExecuting(context);
        }
    }
}
