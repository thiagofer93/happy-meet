﻿using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;

namespace HappyMeet.Middleware
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, UserManager<ApplicationUser> manager)
        {
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');

                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);

                if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    var user = await manager.FindByNameAsync(username);
                    if (user != null && await manager.CheckPasswordAsync(user, password))
                    {
                        await _next(context);
                    }
                    else
                    {

                        context.Response.StatusCode = 401;
                        context.Response.Headers.Add("WWW-Authenticate", "Basic realm=\"realm\"");
                    }
                }
                else
                {
                    context.Response.StatusCode = 401; //Unauthorized
                    context.Response.Headers.Add("WWW-Authenticate", "Basic realm=\"realm\"");
                }
            }
            else
            {
                // no authorization header
                context.Response.StatusCode = 401; //Unauthorized
                context.Response.Headers.Add("WWW-Authenticate", "Basic realm=\"realm\"");
            }
        }
    }
}