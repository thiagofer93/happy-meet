using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using HappyMeet.Domain.Entities;

namespace HappyMeet.Middleware
{
    public class ApiOriginMiddleware
    {
        private readonly RequestDelegate _next;

        public ApiOriginMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IConfiguration configuration)
        {
            string origemValida = configuration.GetSection("OrigemApi").Value;
            var source = context.Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (string.IsNullOrEmpty(origemValida) || origemValida.Equals("0.0.0.0") || origemValida.Equals(source))
            {
                await _next(context);
            }
            else
            {
                context.Response.StatusCode = 403;
            }
        }
    }
}