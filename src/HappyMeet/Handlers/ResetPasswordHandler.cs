﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Enums;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Handlers
{
    public class ResetPasswordHandler : IRequestHandler<ResetPasswordRequest, DomainResult>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IUrlHelper _urlHelper;
        private readonly IMediator _mediator;

        public ResetPasswordHandler(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContext, IUrlHelper urlHelper, IMediator mediator)
        {
            _httpContext = httpContext;
            _userManager = userManager;
            _urlHelper = urlHelper;
            _mediator = mediator;
        }

        public async Task<DomainResult> Handle(ResetPasswordRequest request, CancellationToken cancellationToken)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(request.User);
            var callbackUrl = _urlHelper.ResetPasswordCallbackLink(request.User.Id, code, _httpContext.HttpContext.Request.Scheme);
            //await _mediator.Send(new SendEmailRequest(request.User.Email, subject, message), cancellationToken);

            return DomainResult.Ok();
        }
    }
}
