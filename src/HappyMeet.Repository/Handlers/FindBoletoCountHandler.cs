﻿using MediatR;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HappyMeet.Repository.Handlers
{
    public class FindBoletoCountHandler : IRequestHandler<FindBoletoCountRequest, int>
    {
        private readonly PostgresContext _context;

        public FindBoletoCountHandler(PostgresContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(FindBoletoCountRequest request, CancellationToken cancellationToken)
        {
            return await _context.Boletos.Where(request.Expression).CountAsync(cancellationToken);
        }
    }
}
