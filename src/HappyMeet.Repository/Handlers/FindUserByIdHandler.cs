﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Repository.Handlers
{
    public class FindUserByIdHandler : IRequestHandler<FindUserByIdRequest, DomainResult<ApplicationUser>>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public FindUserByIdHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<DomainResult<ApplicationUser>> Handle(FindUserByIdRequest request, CancellationToken cancellationToken)
        {
            return DomainResult.Ok(await _userManager.FindByIdAsync(request.Id.ToString()));
        }
    }
}
