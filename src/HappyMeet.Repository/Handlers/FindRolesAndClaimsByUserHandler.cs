﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Claims;

namespace HappyMeet.Repository.Handlers
{
    public class FindRolesAndClaimsByUserHandler : IRequestHandler<FindRolesAndClaimsByUserRequest, DomainResult<Tuple<IList<string>, IList<Claim>>>>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public FindRolesAndClaimsByUserHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<DomainResult<Tuple<IList<string>, IList<Claim>>>> Handle(FindRolesAndClaimsByUserRequest request, CancellationToken cancellationToken)
        {
            return DomainResult.Ok(new Tuple<IList<string>, IList<Claim>>(await _userManager.GetRolesAsync(request.User), await _userManager.GetClaimsAsync(request.User)));
        }
    }
}
