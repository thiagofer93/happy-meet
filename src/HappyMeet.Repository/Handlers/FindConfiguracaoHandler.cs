﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Repository.Handlers
{
    public class FindConfiguracaoHandler : IRequestHandler<FindConfiguracaoRequest, DomainResult<Configuracao>>
    {
        private readonly PostgresContext _context;
        public FindConfiguracaoHandler(PostgresContext context)
        {
            _context = context;
        }

        public async Task<DomainResult<Configuracao>> Handle(FindConfiguracaoRequest request, CancellationToken cancellationToken)
        {
            return DomainResult.Ok(await _context.Configuracoes.FirstOrDefaultAsync());
        }
    }
}
