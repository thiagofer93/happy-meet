﻿using MediatR;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HappyMeet.Repository.Handlers
{
    public class FindBancoCountHandler : IRequestHandler<FindBancoCountRequest, int>
    {
        private readonly PostgresContext _context;

        public FindBancoCountHandler(PostgresContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(FindBancoCountRequest request, CancellationToken cancellationToken)
        {
            return await _context.Bancos.Where(request.Expression).CountAsync(cancellationToken);
        }
    }
}
