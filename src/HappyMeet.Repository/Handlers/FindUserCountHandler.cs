﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MediatR;
using HappyMeet.Domain.Requests;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;

namespace HappyMeet.Repository.Handlers
{
    public class FindUserCountHandler : IRequestHandler<FindUserCountRequest, int>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public FindUserCountHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<int> Handle(FindUserCountRequest request, CancellationToken cancellationToken)
        {
            return await _userManager.Users.Where(request.Expression).CountAsync(cancellationToken);
        }
    }
}
