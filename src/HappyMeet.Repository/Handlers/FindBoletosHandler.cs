﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace HappyMeet.Repository.Handlers
{
    public class FindBoletosHandler : IRequestHandler<FindBoletosRequest, DomainResult<List<Boleto>>>
    {
        private readonly PostgresContext _context;

        public FindBoletosHandler(PostgresContext context)
        {
            _context = context;
        }

        public async Task<DomainResult<List<Boleto>>> Handle(FindBoletosRequest request, CancellationToken cancellationToken)
        {
            var query = _context.Boletos.Where(request.SearchExpression).OrderBy(request.SortProperty, request.SortOrder).Page(request.PageSkip, request.PageSize);
            return DomainResult.Ok(await query.ToListAsync(cancellationToken));
        }
    }
}
