﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using HappyMeet.Domain.Util;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Transactions;

namespace HappyMeet.Domain.Services
{
    public class PersistUserHandler : IRequestHandler<PersistUserRequest, DomainResult<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMediator _mediator;
        public PersistUserHandler(UserManager<ApplicationUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }

        public async Task<DomainResult<string>> Handle(PersistUserRequest request, CancellationToken cancellationToken)
        {
            using (var scope =
                new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0,10,0), TransactionScopeAsyncFlowOption.Enabled))
            {
                var user = await _userManager.FindByNameAsync(request.UserName);

                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = request.UserName,
                        Email = request.Email,
                        Nome = request.Nome,
                        Ativo = request.Ativo
                    };
                    var result = await _userManager.CreateAsync(user);

                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, request.Role);
                        if (request.SendEmail)
                            await _mediator.Send(new ResetPasswordRequest(user), cancellationToken);
                    }
                    else
                    {
                        return DomainResult.Fail<string>(String.Join("\n", result.Errors.Select(x => x.Description)));
                    }
                }
                else
                {
                    user.Email = request.Email;
                    user.Nome = request.Nome;
                    user.Ativo = request.Ativo;

                    await _userManager.UpdateAsync(user);

                    if (!String.IsNullOrEmpty(request.Password))
                    {
                        var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                        var result = await _userManager.ResetPasswordAsync(user, token, request.Password);

                        if (!result.Succeeded)
                        {
                            return DomainResult.Fail<string>(
                                String.Join("\n", result.Errors.Select(x => x.Description)));
                        }
                    }
                }

                if (request.Claims != null && request.Claims.Any())
                {
                    var currentClaims = await _userManager.GetClaimsAsync(user);

                    foreach (var claim in currentClaims)
                    {
                        if (!request.Claims.Contains(claim.Value))
                        {
                            await _userManager.RemoveClaimAsync(user, claim);
                        }
                    }

                    foreach (var claim in request.Claims)
                    {
                        await _userManager.AddClaimAsync(user, new Claim(ClaimsCustomType.PERMISSAO, claim));
                    }
                }
                
                scope.Complete();

                return DomainResult.Ok("Usuário incluído com sucesso.");
            }
        }
    }
}
