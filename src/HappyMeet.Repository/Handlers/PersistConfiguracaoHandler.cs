﻿using MediatR;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Repository.Handlers
{
    public class PersistConfiguracaoHandler : IRequestHandler<PersistConfiguracaoRequest, DomainResult<string>>
    {
        protected IMediator _mediator { get; }
        private readonly PostgresContext _context;

        public PersistConfiguracaoHandler(PostgresContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<DomainResult<string>> Handle(PersistConfiguracaoRequest request, CancellationToken cancellationToken)
        {
            var message = "";
            if (request.Configuracao.Id == 0)
            {
                _context.Configuracoes.Add(request.Configuracao);
            }
            message = "Configuração salva com sucesso.";

            await _context.SaveChangesAsync();
            return DomainResult.Ok(message);
        }
    }
}
