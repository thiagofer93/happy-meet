﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HappyMeet.Repository.Handlers
{
    public class FindUsersHandler : IRequestHandler<FindUsersRequest, DomainResult<List<ApplicationUser>>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public FindUsersHandler(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<DomainResult<List<ApplicationUser>>> Handle(FindUsersRequest request, CancellationToken cancellationToken)
        {
            return DomainResult.Ok(await _userManager.Users.Where(request.SearchExpression)
                .OrderBy(request.SortProperty, request.SortOrder)
                .Page(request.PageSkip, request.PageSize).ToListAsync(cancellationToken));
        }
    }
}
