﻿using MediatR;
using HappyMeet.Domain.Entities;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace HappyMeet.Repository.Handlers
{
    public class FindBancosHandler : IRequestHandler<FindBancosRequest, DomainResult<List<Banco>>>
    {
        private readonly PostgresContext _context;
        public FindBancosHandler(PostgresContext context)
        {
            _context = context;
        }

        public async Task<DomainResult<List<Banco>>> Handle(FindBancosRequest request, CancellationToken cancellationToken)
        {
            return DomainResult.Ok(await _context.Bancos.Where(request.SearchExpression)
                .OrderBy(request.SortProperty, request.SortOrder)
                .Page(request.PageSkip, request.PageSize)
                .ToListAsync(cancellationToken));
        }
    }
}
