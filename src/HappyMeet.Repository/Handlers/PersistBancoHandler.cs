﻿using MediatR;
using HappyMeet.Domain.Functional;
using HappyMeet.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HappyMeet.Repository.Handlers
{
    public class PersistBancoHandler : IRequestHandler<PersistBancoRequest, DomainResult<string>>
    {
        protected IMediator _mediator { get; }
        private readonly PostgresContext _context;

        public PersistBancoHandler(PostgresContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<DomainResult<string>> Handle(PersistBancoRequest request, CancellationToken cancellationToken)
        {
            var message = "";
            if (request.Banco.Id == 0)
            {
                _context.Bancos.Add(request.Banco);
                message = "Banco incluído com sucesso.";
            }
            else
            {
                message = "Banco atualizado com sucesso.";
            }

            await _context.SaveChangesAsync();
            return DomainResult.Ok(message);
        }
    }
}
