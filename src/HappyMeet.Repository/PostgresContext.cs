﻿using Microsoft.EntityFrameworkCore;
using HappyMeet.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HappyMeet.Domain.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace HappyMeet.Repository
{
    public class PostgresContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public PostgresContext(DbContextOptions<PostgresContext> options) : base(options)
        {
        }

        public DbSet<Configuracao> Configuracoes { get; set; }
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Boleto> Boletos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ForNpgsqlUseSerialColumns();
            base.OnModelCreating(builder);

            //builder.Entity<Boleto>().HasIndex(x =>
            //        new { x.CpfCnpjBenificiario, x.CpfCnpjPagador, x.NumDocumento, x.NossoNumero, x.DigNossoNumero })
            //    .IsUnique().HasName("ix_boleto_1");

            //builder.Entity<Cliente>().HasIndex(x => x.CpfCnpj).IsUnique().HasName("ix_cliente_1");
            //builder.Entity<Cliente>().HasMany(x => x.EmailsLembrete).WithOne(x => x.Cliente).OnDelete(DeleteBehavior.Cascade);

            //builder.Query<BoletoCarregado>().ToView("t_boleto_carregado");
            //builder.Query<BoletoCarregado>().Property(x => x.CpfCnpjBenificiario)
            //    .HasColumnName("cpf_cnpj_benificiario");
            //builder.Query<BoletoCarregado>().Property(x => x.CpfCnpjPagador)
            //    .HasColumnName("cpf_cnpj_pagador");
            //builder.Query<BoletoCarregado>().Property(x => x.NumDocumento)
            //    .HasColumnName("num_documento");
            //builder.Query<BoletoCarregado>().Property(x => x.NossoNumero)
            //    .HasColumnName("nosso_numero");
            //builder.Query<BoletoCarregado>().Property(x => x.DigNossoNumero)
            //    .HasColumnName("dig_nosso_numero");

            foreach (var entity in builder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.Relational().TableName.ToSnakeCase();

                foreach (var property in entity.GetProperties())
                {
                    property.Relational().ColumnName = property.Name.ToSnakeCase();
                }

                foreach (var key in entity.GetKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.Relational().Name = index.Relational().Name.ToSnakeCase();
                }
            }
        }
    }
}
